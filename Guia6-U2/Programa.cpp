#include <iostream>
#include <string>
#include <climits>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include "Dijkstra.h"
using namespace std;

int main(int argc, char *argv[]){
    int n;
    n = atoi(argv[1]);
    if (n <= 2 || n >= 100){
        cout << "Valor invalido." << endl;
        return -1;
    }
    else {
        char V[n], S[n], VS[n]; // [V] Vértices, [S] Distancia de origen a un nodo, [VS] Mezcla de ambos
        int D[n]; // Distancia menor
        int **matriz_aux;
        matriz_aux = new int*[n];
        for (int i = 0; i < n; i++){
            matriz_aux[i] = new int[n];
        }
        Dijkstra dijks;
        int tecla = 0;
        string tecla_op = "\0";
        int bandera = true;
        
        do {
            cout << endl;
            cout << "|------------- MENU -------------|" << endl;
            cout << "| [1] Ingreso y muestra de datos.|" << endl;
            cout << "| [2] Muestra grafo.             |" << endl;
            cout << "| [3] Salir.                     |" << endl;
            cout << "|--------------------------------|" << endl;
            cout << " > ¿Qué opción toma?: ";
            cin >> tecla_op;
            tecla = atoi(tecla_op.c_str());
            cin.ignore();

            if (tecla == 1){
                dijks.inicializar_vector_caracter(V, n);
                dijks.inicializar_vector_caracter(S, n);
                dijks.inicializar_vector_caracter(VS, n);
                dijks.inicializar_matriz_enteros(matriz_aux, n); 
                dijks.agregar_nombres(n, V, matriz_aux);
                dijks.rellenar_matriz(n, V, matriz_aux);
                dijks.imprimir_matriz(matriz_aux, n);
                dijks.aplicar_dijkstra(V, S, VS, D, matriz_aux, n);
            }
            else if (tecla == 2){
                dijks.imprimir_grafo(matriz_aux, V, n);
            }
            else if (tecla == 3){
                cout << "\nPrograma finalizado." << endl;
                /* Salir del programa */
                bandera = false;
            }
            else {
                cout << "\nTecla incorrecta." << endl;
            }
        } while (bandera == true);
    }
    return 0;
};