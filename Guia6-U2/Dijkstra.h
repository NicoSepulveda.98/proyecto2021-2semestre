#ifndef DIJKSTRA_H
#define DIJKSTRA_H

#include <iostream>
#include <cstring>
#include <climits>
#include <cstdio>
#include <cstdlib>
#include <fstream>
using namespace std;
#define MAX 100
#define Inf 0x3f3f3f3f

class Dijkstra{
    /* Atributos privados */
    private:
    int **matriz; /* Matriz */
    char nodoID; /* Nombre nodo */
    int dis; /* Distancia de nodos */

    public:
    /* Constructor */
    Dijkstra();
    /* Métodos a utilizar */
    void inicializar_vector_caracter(char *vector, int n);
    void inicializar_vector_D(int *D, int **matriz, int n);
    void inicializar_matriz_enteros(int **matriz, int n);
    void agregar_nombres(int total_nodos, char nombres[], int **matriz);
    void rellenar_matriz(int total_nodos, char nombres[], int **matriz);
    void imprimir_vector_caracter(char *vector, int n);
    void imprimir_vector_enteros(int *vector, int n);
    void imprimir_matriz(int **matriz, int n);
    void imprimir_grafo(int **matriz, char *vector, int n);
    void aplicar_dijkstra(char *V, char *S, char *VS, int *D, int **matriz, int n);
    int buscar_indice_caracter(char *V, char caracter, int n);
    int busca_caracteres(char c, char *vector, int n);
    void actualizar_pesos(int *D, char *VS, int **matriz, char *V, char v, int n);
    void agrega_vertice_a_S(char *S, char vertice, int n);
    void actualizar_VS(char *V, char *S, char *VS, int n);
    char elegir_vertice(char *VS, int *D, char *V, int n);
    int calcular_minimo(int dw, int dv, int mvw);
    

};

#endif