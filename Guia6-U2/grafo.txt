digraph G {
graph [rankdir=LR]
node [style=filled fillcolor=yellow];
A->B [label=4];
A->C [label=11];
B->D [label=6];
B->E [label=2];
C->B [label=3];
C->D [label=6];
E->C [label=5];
E->D [label=3];
}
