#include <iostream>
#include <climits>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include "Dijkstra.h"
using namespace std;


Dijkstra::Dijkstra(){
}

// Inicializa un vector. recibe el vector como un puntero.
void Dijkstra::inicializar_vector_caracter(char *vector, int n) {
    int col;
    // Recorre el vector.
    for (col=0; col<n; col++) {
        vector[col] = ' ';
    }
}
// Imprime un vector. recibe el vector como un puntero.
void Dijkstra::imprimir_vector_caracter(char *vector, int n) {
    cout << endl;
    for (int i=0; i<n; i++) {
        cout << "vector[" << i << "]: " << vector[i] << " ";
    }
    cout << endl;
}

// Inicializa matriz nxn. recibe puntero a la matriz.
void Dijkstra::inicializar_matriz_enteros (int **matriz, int n) {
    for (int fila=0; fila<n; fila++) {
        for (int col=0; col<n; col++) {
            matriz[fila][col] = -1;
        }
    }
}

void Dijkstra::inicializar_vector_D(int *D, int **matriz, int n) {
    int col;
    
    for (col=0; col<n; col++) {
        D[col] = matriz[0][col];
    }
}

// Imprime matriz.
void Dijkstra::imprimir_matriz(int **matriz, int n) {
    cout << endl;
    cout << "--> Matriz Impresa <-- " << endl << endl;
    for (int fila=0; fila<n; fila++) {
        for (int col=0; col<n; col++) {
            cout << matriz[fila][col] << "  ";
        }
        cout << endl;
    }
}

void Dijkstra::imprimir_vector_enteros(int *vector, int n){
    int i;
    cout << endl;
    for (i=0; i<n; i++) {
        cout << "D["<< i <<"]: " << vector[i] << endl;
    }
    
}

void Dijkstra::agregar_nombres(int total_nodos, char nombres[], int **matriz){
    int contador = 0;
    while (contador < total_nodos){
        cout << "Ingrese nombre de nodo " << contador+1 << ": ";
        cin >> nodoID;
        nombres[contador] = nodoID;
        contador++;
    }
}

void Dijkstra::rellenar_matriz(int total_nodos, char nombres[], int **matriz){
    int i = 0;
    int j = 0;
    for (i = 0; i < total_nodos; i++){  
        cout << "\nTrabajando con nodo: " << nombres[i] << endl;
        for (j = 0; j < total_nodos; j++){
            if (i != j){ 
                cout << " > Digite la distancia entre nodos " << nombres[i] << " hasta " << nombres[j] << ": ";
                cin >> dis;
                matriz[i][j] = dis;
            }
            else {
                matriz[i][j] = 0;
            }
        }
    }
    /*
    cout << "\n-- MATRIZ INICIAL --" << endl;
    for (int i = 0; i < total_nodos; i++){
        for (int j = 0; j < total_nodos; j++){
            cout << matriz[i][j] << " ";
        }
        cout << endl << endl;
    }
    */
}

void Dijkstra::agrega_vertice_a_S(char *S, char vertice, int n){
    int i;
    
    // Recorre buscando un espacio vacio.
    for (i=0; i<n; i++) {
        if (S[i] == ' ') {
            S[i] = vertice;
            return;
        }
    } 
}

void Dijkstra::actualizar_VS(char *V, char *S, char *VS, int n) {
    int j;
    int k = 0;
    
    inicializar_vector_caracter(VS, n);
    
    for (j=0; j<n; j++){
        // Por cada caracter de V[] evalua si está en S[],
        // Sino está, lo agrega a VS[].
        if (busca_caracteres(V[j], S, n) != true) {
            VS[k] = V[j];
            k++;
        }
    }
}

int Dijkstra::busca_caracteres(char c, char *vector, int n) {
    int j;
    
    for (j=0; j<n; j++) {
        if (c == vector[j]) {
        return true;
        }
    }
    return false;
}

char Dijkstra::elegir_vertice(char *VS, int *D, char *V, int n){
    int i = 0;
    int menor = 0;
    int peso;
    char vertice;

    while (VS[i] != ' ') {
        
        peso = D[buscar_indice_caracter(V, VS[i], n)];
        // Descarta valores infinitos, 0 y (-1)
        if ((peso != -1) && (peso != 0)) {
            if (i == 0) {
                menor = peso;
                vertice = VS[i];
            } 
            
            else {
                if (peso < menor) {
                    menor = peso;
                    vertice = VS[i];
                }
            }
        }

        i++;
    }
    
    cout << "\nVertice menor: " << vertice << endl;
    return vertice;

}

int Dijkstra::buscar_indice_caracter(char *V, char caracter, int n) {
    int i;
    
    for (i=0; i<n; i++) {
        if (V[i] == caracter)
        return i;
    }
    
    return i;
}

void Dijkstra::actualizar_pesos(int *D, char *VS, int **matriz, char *V, char v, int n) {
    int i = 0;
    int indice_w, indice_v;

    cout << "\n> actualiza pesos en D[]" << endl;
    
    indice_v = buscar_indice_caracter(V, v, n);
    while (VS[i] != ' ') {
        if (VS[i] != v) {
            indice_w = buscar_indice_caracter(V, VS[i], n);
            D[indice_w] = calcular_minimo(D[indice_w], D[indice_v], matriz[indice_v][indice_w]);
        }
        i++;
    }
}

int Dijkstra::calcular_minimo(int dw, int dv, int mvw) {
    int min = 0;

    if (dw == -1) {
        if (dv != -1 && mvw != -1)
        min = dv + mvw;
        else
        min = -1;

    } 

    else {
        if (dv != -1 && mvw != -1) {
            if (dw <= (dv + mvw)){
                min = dw;
            }
            else{
                min = (dv + mvw);
            }
        }
        else{
            min = dw;
        }
    }
    
    cout << "\ndw: " << dw;  
    cout << "\ndv: " << dv;
    cout << "\nmvw: " << mvw;
    cout << "\nmin: " << min;
    cout << endl;

    return min;
}

void Dijkstra::aplicar_dijkstra(char *V, char *S, char *VS, int *D, int **matriz, int n){
    int i;
    char v;
    
    // Inicializar vector D[] segun datos de la matriz 
    cout << "\n> Se inicializa el vector D" << endl;
    inicializar_vector_D(D, matriz, n);
    cout << endl;
    imprimir_vector_enteros(D, n);

    // Impresión de datos antes de aplicar el algoritmo
    cout << "\n> Impresion de datos antes de aplicar el algoritmo" << endl;
    cout << "\nVector V: ";
    imprimir_vector_caracter(V, n);
    cout << "\nVector S: ";
    imprimir_vector_caracter(S, n);
    cout << "\nVector VS: ";
    imprimir_vector_caracter(VS, n); 

    // Agrega el primer véctice.
    cout << "\n> Se agrega el primer valor V[0] a S[] y actualiza VS[]" << endl;
    agrega_vertice_a_S(S, V[0], n);
    cout << "\nVector S: ";
    imprimir_vector_caracter(S, n);
    actualizar_VS(V, S, VS, n);

    cout << "\nVector VS: ";
    imprimir_vector_caracter(VS, n);
    cout << "\nVector VS: ";
    imprimir_vector_enteros(D, n);

    for (i=1; i<n; i++) {
        // Elige un vértice en v de VS[] tal que D[v] sea el mínimo 
        cout << "\n> Se elige el vertice menor en VS[] según valores en D[] y lo agrega a S[] y actualiza VS[]" << endl;
        v = elegir_vertice(VS, D, V, n);

        agrega_vertice_a_S(S, v, n);
        imprimir_vector_caracter(S, n);

        actualizar_VS(V, S, VS, n);
        imprimir_vector_caracter(VS, n);

        actualizar_pesos(D, VS, matriz, V, v, n);
        imprimir_vector_enteros(D, n);
    }
    cout << endl;
}

void Dijkstra::imprimir_grafo(int **matriz, char *vector, int n) {
    int i, j;
    FILE *fp;
    fp = fopen("grafo.txt", "w");
    fprintf(fp, "%s\n", "digraph G {");
    fprintf(fp, "%s\n", "graph [rankdir=LR]");
    fprintf(fp, "%s\n", "node [style=filled fillcolor=yellow];");
    
    for (i=0; i<n; i++) {
        for (j=0; j<n; j++) {
            // Evalúa la diagonal principal.
            if (i != j) {
                if (matriz[i][j] > 0) {
                    fprintf(fp, "%c%s%c [label=%d];\n", vector[i],"->", vector[j], matriz[i][j]);
                }
            }
        }
    }
    
    fprintf(fp, "%s\n", "}");
    fclose(fp);

    system("dot -Tpng -ografo.png grafo.txt");
    system("eog grafo.png &");
}