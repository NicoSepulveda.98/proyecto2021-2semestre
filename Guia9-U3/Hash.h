#include <iostream>
using namespace std;

#ifndef HASH_H
#define HASH_H

typedef struct _nodo{ // Estructura del nodo
	int numero;
	struct _nodo *sig;
} nodo;


class Hash {
    private:

    public:
		/* Constructor */
		Hash ();
		void colisionesPruebaLineal(int posicion, int* array, int tamano, int aux);
		void colisionesPruebaCuadratica(int posicion, int* array, int tamano, int aux);
		void colisionesDobleDireccion(int posicion, int* array, int tamano, int aux);
		void colPruebaLinealInsercion(int posicion, int* array, int tamano, int aux);
		void colPruebaCuadraticaInsercion(int posicion, int* array, int tamano, int aux);
		void colDobleDireccionInsercion(int posicion, int* array, int tamano, int aux);
		void imprimirColision(int* array, int tamano);
		/* Voids para encadenamiento */
		void ArregloCopia(nodo **array1, int* array, int tamano);
		void imprimirLista(nodo **array, int posicion);
		void metodoEncadenamiento(int* array, int tamano, int aux, int posicion);

};

#endif