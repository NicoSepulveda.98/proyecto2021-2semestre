#include <iostream>
#include "Hash.h"

using namespace std;


/* Se pide ingreso del tamaño del arreglo y los valores de este */
int CreacionArray(int aux){
	int num;
	//system("clear");
	cout << endl;
	/* Al iniciar programa aux vale 0 por ende ejecutará esta línea de código */
	if(aux == 0){
		cout << "Ingrese el tamaño del arreglo: ";
	}
	/* Cuando se ingrese al menu aux valdrá 1 y se podrá ingresar números al arreglo */
	else if(aux == 1){
		cout << "Ingrese un número: ";
	}
	cin >> num;
	/* Se retorna al valor ingresado */
	return num;
}

/* Función que manipula el arreglo */
int ManejoArreglo(int* array, int tamano, int aux){
	int contador = 0;

	if(aux == 1){
		cout << "\nSu arreglo es: " << endl << endl;
	}
	for(int i = 0; i < tamano; i++){
		if(aux == 0){
			array[i] = '\0';
		}
		if(aux == 1){
			cout << i << "  [" << array[i] << "]" << endl;
		}
		if(aux == 2 && array[i] != '\0'){
			contador = contador + 1;
		}
	}
	if(aux == 2){
		if(contador > 0){
			return -1;
		}
		else{ 
			return 999999;
		}
	}
	cout << endl << endl;

	return 0;
}

void ColisionesInsercion(int* array, int tamano, int aux, string metodoColision, int posicion, Hash hash){
    if(metodoColision.compare("L") == 0){
		hash.colPruebaLinealInsercion(posicion, array, tamano, aux);
    }

    else if(metodoColision.compare("C") == 0){
		hash.colPruebaCuadraticaInsercion(posicion, array, tamano, aux);
    }

    else if(metodoColision.compare("D") == 0){
		hash.colDobleDireccionInsercion(posicion, array, tamano, aux);
    }

    else if(metodoColision.compare("E") == 0 ){
        hash.metodoEncadenamiento(array, tamano, aux, posicion);
    }  
}

void ColisionesBusqueda(int* array, int tamano, int aux, string metodoColision, int posicion, Hash hash){
    if(metodoColision.compare("L") == 0){
        hash.colisionesPruebaLineal(posicion, array, tamano, aux);
    }

    else if(metodoColision.compare("C") == 0){
        hash.colisionesPruebaCuadratica(posicion, array, tamano, aux);
    }

    else if(metodoColision.compare("D") == 0){
        hash.colisionesDobleDireccion(posicion, array, tamano, aux);
    }

    else if(metodoColision.compare("E") == 0 ){
        hash.metodoEncadenamiento(array, tamano, aux, posicion);
    }
}

/* Se asigna la posición donde se agregara el dato */
void funcionHash(int* array, int tamano, int aux, string metodoColision, Hash hash){ 
    int posicion;
    /* Asigna posición para guardar el dato */
    posicion = (aux%(tamano - 1)) + 1;
    
	/* Existe colisión */ 
    if(array[posicion] != '\0'){
        cout << "Colision en posición: " << "[" << posicion << "]" << endl;
        /* Se revisa el método a utilizar */
        ColisionesInsercion(array, tamano, aux, metodoColision, posicion, hash); 
        ManejoArreglo(array, tamano, 1);
    }
    else{
        array[posicion] = aux;
        ManejoArreglo(array, tamano, 1);
    }
}

/*cFunción encargada de interactuar con el usuario */
void menu(int* array, int tamano, string metodoColision){
	/* Variables utilizables */
	Hash hash = Hash();
	int posicion;
	int opcion;
	int aux = 0;
	int aux2 = 0;
	bool bandera = true;
	//system("clear"); // Limpiar terminal
	ManejoArreglo(array, tamano, 0);

	while(bandera){
		cout << "|--------------MENU--------------|" << endl;
		cout << "| [1] Insertar Numero al Arreglo |" << endl;
		cout << "| [2] Buscar Número al Arreglo   |" << endl;
		cout << "| [0] Salir                      |" << endl;
		cout << "|--------------------------------|" << endl << endl;
		cout << "¿Qué opción desea tomar: ";
		cin >> opcion;

		if (opcion == 1){
			aux = CreacionArray(1);
			funcionHash(array, tamano, aux, metodoColision, hash);
		}
		else if (opcion == 2){
			/* Se revisa si el arreglo se encuentra lleno */
			aux2 = ManejoArreglo(array, tamano, 2);
			if(aux2 == -1){
                aux = CreacionArray(1);
                posicion = (aux%(tamano - 1)) + 1;
				/* Si se encuentra el número */
                if(array[posicion] == aux){
                    cout << "[" << aux << "]" << " Se encontró en la posición " << "[" << posicion << "]" << endl;
                    ManejoArreglo(array, tamano, 1);
                }
                else{
                    cout << "Colisión en la posición " << "[" << posicion << "]" << endl;
					ColisionesBusqueda(array, tamano, aux, metodoColision, posicion, hash);
                }
            }
            /* Si el arreglo no posee elementos, avisa que esta vacío */
            else{
                cout << "\nArreglo vacío [] ..." << endl << endl;
            }
		}
		else if (opcion == 0){
			/* Se sale del programa ya que bandera es false */
			cout << "\n¡¡PROGRAMA FINALIZADO!!" << endl << endl;
			bandera = false;
		}
		else {
			cout << "Ingrese un número válido" << endl;
		}
	}
}

/* Se crea el main */
int main(int argc, char *argv[]){
	string metodoColision;
	int tamano = 0;
	int *array;
	
	if(argc == 2){
		metodoColision = argv[1];
		if((metodoColision == "L" || metodoColision == "l") ||(metodoColision == "C" || metodoColision == "c") || (metodoColision == "D" || metodoColision == "d") || (metodoColision == "E" || metodoColision == "e")){
			tamano = CreacionArray(0);
			if (tamano > 0){
				array = new int[tamano];
			}
			else{
				cout << "Por favor, crear un arreglo mayor de 0" << endl;
				exit(-1);
			}
			menu(array, tamano, metodoColision);
		}
		else{
			cout << "Ingrese un cáracter válido" << endl;
			exit(-1);
		}
	}
	else{
		cout << "¡Parámetros inválidos!" << endl;
		return 1;
	}
}
