#include "Hash.h"
#include <iostream>

using namespace std;

/* Constructor vacío */
Hash::Hash(){
}
/* Imprimir Colisión */
void Hash::imprimirColision(int* array, int tamano){
	/* Recorrer array e imprime posición y valor */
    for(int i = 0; i < tamano; i++){
        cout << "[" <<  i << "]"  " [" << array[i] << "]"<< endl;
    }
}

void Hash::colisionesPruebaLineal(int posicion, int *array, int tamano, int aux){
    int nuevaPosicion;

    if(array[posicion] != '\0' && array[posicion] == aux){
        cout << "[" <<  aux << "]" << " Se encontró en la posición " << "[" << posicion << "]" << endl;
    }
	else{
        nuevaPosicion = posicion + 1;
        while(array[nuevaPosicion] != '\0' && nuevaPosicion <= tamano && nuevaPosicion != posicion && array[nuevaPosicion] != aux){
            nuevaPosicion = nuevaPosicion + 1;
            /* Vuelve a empezar*/
            if(nuevaPosicion == tamano){
                nuevaPosicion = 0;
            }
        }
        if(array[nuevaPosicion] == '\0' || nuevaPosicion == posicion){
            cout << "[" <<  aux << "]" << " No se encontró dentro del array]" << endl;
            imprimirColision(array, tamano);
        }
        else{
            cout << "[" <<  aux << "]" << " Se encontró en la posición " << "[" << nuevaPosicion << "]"<< endl;
            imprimirColision(array, tamano);
        }
    }
}

void Hash::colisionesPruebaCuadratica(int posicion, int *array, int tamano, int aux){
    int i;
    int nuevaPosicion;

    if(array[posicion] != '\0' && array[posicion] == aux){
        cout << "[" << aux << "]" << " Se encontró en la posición " << "[" << posicion << "]"<< endl;
    }
    else{
        i = 0;
        nuevaPosicion = (posicion + (i * i)); 
        /* Determina si la nueva posición esta vacía */
        while(array[nuevaPosicion] != '\0' && array[nuevaPosicion] != aux){ 
            i = i + 1; 
            /* Nueva posición */
            nuevaPosicion = (posicion + (i * i)); 

            if(nuevaPosicion > tamano){ 
                i = -1;
                /* Reiniciar la posición */
                nuevaPosicion = 1;
                posicion = 0;
            }
        }
        if(array[nuevaPosicion] == '\0'){
            cout << "[" <<  aux << "]" << " No existe dentro del array" << endl;
            imprimirColision(array, tamano);
        }
        else{
            cout << "[" <<  aux << "]" << " Se encontró en la posición " << "[" << nuevaPosicion << "]" << endl;
            imprimirColision(array, tamano);
        }
    }
}

void Hash::colisionesDobleDireccion(int posicion, int *array, int tamano, int aux){
    int nuevaPosicion;

    if(array[posicion] != '\0' && array[posicion] == aux){
        cout << "[" <<  aux << "]" << " Se encontró en la posición " << "[" << posicion << "]" << endl;
    }
    else{
        nuevaPosicion = ((posicion + 1)%tamano - 1) + 1;
        while(nuevaPosicion <= tamano && array[nuevaPosicion] != '\0' && nuevaPosicion != posicion && array[nuevaPosicion] != aux){
            nuevaPosicion = ((nuevaPosicion + 1)%tamano -1) + 1; 
        }

        if(array[nuevaPosicion] == '\0' || (nuevaPosicion == posicion)){ 
            cout << "[" <<  aux << "]" << " No se encuentra en el arreglo" << endl;
            imprimirColision(array, tamano);
        }
        else{
            cout << "[" <<  aux << "]" << " Se encontró en la posición " << "[" << nuevaPosicion << "]" << endl;
            imprimirColision(array, tamano);
        }
    }
}

void Hash::colPruebaLinealInsercion(int posicion, int* array, int tamano, int aux){
    int nuevaPosicion;
    int contador;

    if(array[posicion] != '\0' && array[posicion] == aux){ 
        cout << "[" <<  aux << "]" << " Se encontró en la posición " << "[" << posicion << "]" << endl;
    }
    else{
        contador = 0;
        nuevaPosicion = posicion + 1; 
        while(array[nuevaPosicion] != '\0' && nuevaPosicion <= tamano && nuevaPosicion != posicion && array[nuevaPosicion] != aux){
            nuevaPosicion = nuevaPosicion + 1;
            if(nuevaPosicion == tamano){
                nuevaPosicion = 0; 
            }
            contador = contador + 1; 
        }
		/* Determinar si existen o no, espacios en el arreglo */
        if(contador == tamano){ 
            cout << "Sin espacios en el arreglo" << endl;
        }
        if(array[nuevaPosicion] == '\0'){
            array[nuevaPosicion] = aux;
            cout << "[" << aux << "]" << " Se movió a la posición " << "[" << nuevaPosicion << "]\n" << endl;
            imprimirColision(array, tamano);
            contador = contador + 1;
        }
    }
}

void Hash::colPruebaCuadraticaInsercion(int posicion, int* array, int tamano, int aux){
    int i;
    int nuevaPosicion;

	/* Determina si el número se encuentra en el arreglo */
    if(array[posicion] != '\0' && array[posicion] == aux){ 
		cout << "[" << aux << "]" << " Se encontró en la posición " << "[" << posicion << "]" << endl;
    }
    /* Si no es así */
    else{
        i = 0;
        /* Se avanza posiciones cuadráticamente */
        nuevaPosicion = (posicion + (i * i)); 
        while(array[nuevaPosicion] != '\0' && array[nuevaPosicion] != aux){ 
            i = i + 1;
            /* Nueva posición */
            nuevaPosicion = (posicion + (i * i)); 
            if(nuevaPosicion > tamano){ 
                i = -1;
                /* Posición 0 */
                nuevaPosicion = 1; 
                posicion = 0;
            }
        }
        /* El número se movió */
        if(array[nuevaPosicion] == '\0'){
            array[nuevaPosicion] = aux;
            cout << "[" << aux << "]" << " Se movió a la posicion " << "[" << nuevaPosicion << "]" << "\n" << endl;
            imprimirColision(array, tamano);
        }
    }
}

void Hash::colDobleDireccionInsercion(int posicion, int* array, int tamano, int aux){ 
    int nuevaPosicion;

    if(array[posicion] != '\0' && array[posicion] == aux){ 
        cout << "[" << aux << "]" << " Se encontró en la posición " << "[" << posicion << "]" << endl;
    }
    else{
        nuevaPosicion = ((posicion + 1)%tamano - 1) + 1; 
        while(nuevaPosicion <= tamano && array[nuevaPosicion] != '\0' && nuevaPosicion != posicion && array[nuevaPosicion] != aux){
            nuevaPosicion = ((nuevaPosicion + 1)%tamano - 1) + 1;
        }
        /* Si la nueva posición esta vacía */
        if(array[nuevaPosicion] == '\0'){ 
            array[nuevaPosicion] = aux;
            /* Si existe o no, espacio en el arreglo */
            if(nuevaPosicion >= tamano){
                cout << "Sin espacio en el arreglo" << endl;
                nuevaPosicion = 0;
            }
            else{
                cout << "[" << aux << "]" << " Se movió a la posición " << "[" << nuevaPosicion << "]" << "\n" << endl;
                imprimirColision(array, tamano);
            }
        }
    }
}

/* Voids para encadenamiento */
void Hash::ArregloCopia(nodo **array1, int *array, int tamano){
    for(int i = 0; i < tamano; i++){
        array1[i]->numero = array[i];
        array1[i]->sig = NULL;
    }
}

void Hash::imprimirLista(nodo **array, int posicion){
    nodo *aux = NULL;
    aux = array[posicion];
	while(aux != NULL){
        if(aux->numero != 0){
            cout << "[" << aux->numero << "] - " << endl;
        }
		aux = aux->sig;
    }
}

void Hash::metodoEncadenamiento(int *array, int tamano, int aux, int posicion){
    nodo *temporal = NULL;
    nodo *arregloAux[tamano];
    
    for(int i = 0; i < tamano; i++){
        arregloAux[i] = new nodo();
		arregloAux[i]->sig = NULL;
		arregloAux[i]->numero = '\0';
    }
    ArregloCopia(arregloAux, array, tamano);

    if(array[posicion] != '\0' && array[posicion] == aux){
        cout << "[" << aux << "]" << " Se encontró en la posición " << "[" << posicion << "]" << endl;
    }
    else{
        temporal = arregloAux[posicion]->sig;
        while(temporal != NULL && temporal->numero != aux){
            temporal = temporal->sig;
        }
        if(temporal == NULL){
            cout << "[" << aux << "]" << " No está en la lista" << endl;
            cout << "Arreglo: "  << endl;
            imprimirLista(arregloAux, posicion);
        }
		else{
            cout << "[" << aux << "]" << " Se encontró en la posición " << "[" << posicion << "]" << endl;
            cout << "\nArreglo: "  << endl;
            imprimirLista(arregloAux, posicion);
        }
    } 
}
