**Ejercicio Guia9-U3**

El siguiente programa consiste en el ingreso y busqueda de información, especificamente, números dentro de un arreglo. Se utilza métodos como reasignación Prueba Lineal, reasignación Prueba Cuadrática, reasignación Doble Dirección Hash, Encadenamiento. 

**Especificaciones técnicas**

El método de resolución es mediante la terminal, es decir, al ejecutar el programa con ./Programa debe ingresar un parámetro que puede ser **L**, **C**, **D**, **E**. Una vez ejecutado el programa debe ingresar el tamaño del arreglo y luego lo dirigirá al menu del programa, este consta de un ingreso de números, busqueda de números y una opción de salida. Cabe decir, que si el arreglo esta vacío el programa se lo dirá. Por favor, el programa solo consta para números enteros, cualquier variable de otro tipo (string, char, entre otros) producirá un error.

**Historia:**

Fue escrito y desarrollado por Nicolás Sepúlveda Falcón, utilizando para ello el lenguaje de programación C++. Se trata de un lenguaje robusto fuertemente equipado y que sigue un paradigma de programación orientada a objetos.

**Para empezar:**

Es requisito tener instalado **C++**, de preferencia compilador **g++ 9.3.0**, previamente en su computadora, de lo contrario, el programa no podrá ser lanzado.
Recomendamos que el sistema operativo sobre el cual pretende lanzar el programa, corra sobre el kernel Linux (Debian, Ubuntu, Arch, entre otras.)

**Instalación:**

Para instalar y ejecutar el programa en su máquina, es necesario que siga las presentes indicaciones:
1- Clonar el repositorio "proyecto2021-2semestre" en el directorio de su preferencia, el enlace HTTPS para clonar el programa ingresando a la terminal es el siguiente:
```
git clone https://gitlab.com/NicoSepulveda.98/proyecto2021-2semestre.git
```
2- Luego, deberá entrar en la carpeta clonada que lleva por nombre "proyecto2021-2semestre" especificamente en "Guia9-U3" que contiene los ejercicios con sus respectivas clases en formato ".cpp" y ".h" y un Makefile.
3- Posteriormente, ya está usted preparado para ejecutar el programa. Puede ser lanzado desde una consola de su IDE de preferencia o desde una terminal en Linux

**Codificación:**

El programa soporta la codificación estándar UTF-8

**Construido con:**

Visual Studio Code, IDE utilizado por defecto para el desarrollo del proyecto.
Sitio web para descargar para Linux, Windows y Mac en https://code.visualstudio.com/download"

**Licencia:**

Este proyecto está sujeto bajo la Licencia GNU GPL v3.

**Autores y creadores del proyecto:**

> Nicolás Sepúlveda Falcón.

