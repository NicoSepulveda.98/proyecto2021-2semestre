#include "Cliente.h"
#include <iostream>
using namespace std;

/* Constructor */
Cliente::Cliente(){

}

/* Getter's & Setter's */
string Cliente::get_nombre(){
    return this->nombre;
}

string Cliente::get_telefono(){
    return this->telefono;
}

int Cliente::get_saldo(){
    return this->saldo;
}

bool Cliente::get_moroso(){
    return this->moroso;
}

void Cliente::set_nombre(string nombre){
    this->nombre = nombre;
}

void Cliente::set_telefono(string telefono){
    this->telefono = telefono;
}

void Cliente::set_saldo(int saldo){
    this->saldo = saldo;
}

void Cliente::set_moroso(bool moroso){
    this->moroso = moroso;
}