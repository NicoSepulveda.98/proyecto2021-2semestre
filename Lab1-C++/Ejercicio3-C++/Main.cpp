#include "Cliente.h"
#include <iostream>
using namespace std;

int main(){
    int n_clientes;
    cout << "Ingrese numero de clientes: ";
    cin >> n_clientes;
    /* Arreglo de objetos y creación de variables */
    Cliente cliente[n_clientes];
    string nombre;
    string numero;
    int saldo;

    for(int i = 0; i < n_clientes; i++){
        cout << "Ingrese nombre del cliente: ";
        cin >> nombre;
        cout << "Ingrese numero cliente: ";
        cin >> numero;
        cout << "Ingrese saldo (deuda) cliente: ";
        cin >> saldo;
        cliente[i].set_nombre(nombre);
        cliente[i].set_telefono(numero);
        cliente[i].set_saldo(saldo);
        if(cliente[i].get_saldo() != 0){
            cliente[i].set_moroso(true);
        }
    }
    system("clear");
    int contador_moroso = 0;
    int contador_sindeuda = 0;
    for(int j = 0; j < n_clientes; j++){
        if(cliente[j].get_saldo() != 0){
            cout << " >> Cliente moroso  << " << endl;
            cout << cliente[j].get_nombre() << ", ";
            cout << "Telefono: " << cliente[j].get_telefono() << ", ";
            cout << "Saldo pendiente: " << cliente[j].get_saldo();
            contador_moroso++;
            cout << "\n\n";
        }
        else if(cliente[j].get_saldo() == 0){
            cout << " >> Cliente sin deuda <<" << endl;
            cout << cliente[j].get_nombre() << ", ";
            cout << "Telefono: " << cliente[j].get_telefono() << ", ";
            cout << "Saldo pendiente: " << cliente[j].get_saldo();
            contador_sindeuda++;
            cout << "\n\n";
        }
    }
    cout << "Total de clientes morosos: " << contador_moroso << endl;
    cout << "Total de clientes sin deuda: " << contador_sindeuda << endl;

    return 0;
    
}
