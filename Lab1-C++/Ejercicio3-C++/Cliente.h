#include <iostream>
using namespace std;

#ifndef CLIENTE_H
#define CLIENTE_H

class Cliente{
    private:
    string nombre = "\0";
    string telefono = "\0";
    int saldo;
    bool moroso = false;

    public:
    /* Constructor */
    Cliente();
    /* Getter's */
    string get_nombre();
    string get_telefono();
    int get_saldo();
    bool get_moroso();
    /* Setter's */
    void set_nombre(string nombre);
    void set_telefono(string telefono);
    void set_saldo(int saldo);
    void set_moroso(bool moroso);

};

#endif