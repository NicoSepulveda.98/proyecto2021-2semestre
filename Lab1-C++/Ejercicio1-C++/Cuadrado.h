#include <iostream>
#include <list>
using namespace std;
#ifndef CUADRADO_H
#define CUADRADO_H

class Cuadrado{
    private:
    /* Atributos */
    int numero = 0;
    int *arreglo = NULL;
    int t = 0;

    public:
    /* Constructor */
    Cuadrado(int numero);
    void llenar_arreglo();
    void calcular_cuadrado();
    int get_t();
};

#endif 