#include "Cuadrado.h"
#include <iostream>
#include <stdlib.h>
using namespace std;

int main(void){
    /* Pedir usuario el tamaño de matriz */
    int aux_n;
    cout << "Ingrese el tamaño del arreglo en número entero: ";
    cin >> aux_n;
    /* Instanciar objeto y pasar como parametro -> aux_n */
    Cuadrado c = Cuadrado(aux_n);
    c.llenar_arreglo();
    c.calcular_cuadrado();
    /* Obtener suma */
    cout << "\n\nSuma total de los cuadrados es: " << c.get_t() << endl;
    return 0;
}