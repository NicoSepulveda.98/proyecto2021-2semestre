#include "Letra.h"
#include <iostream>
using namespace std;

void pedir_tamano(){
    int tamano;
    cout << "¿Cuántas palabras ingresará?: ";
    cin >> tamano;
    cin.ignore();
    /* Instanciar obejto */
    Letra letra = Letra(tamano);
    letra.contador_diferenciador();
}

int main(void){
    pedir_tamano();
    return 0;
}