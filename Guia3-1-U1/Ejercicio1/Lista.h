#include <iostream>
#include <stdlib.h>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

/* Crear nodo */
struct Nodo{
    int dato;
    Nodo *next = NULL;
};

class Lista{
    /* Atributos */
    private:
    int dato;
    Nodo *lista = NULL;

    /* Constructor */
    public:
    Lista();
    void recibir(int dato);
    void recibirOrden(int dato);
    void muestra();
    void insertar(Nodo *&lista, int num);
    void insertarOrden(Nodo *&lista, int num);
    int getDato();
};

#endif
