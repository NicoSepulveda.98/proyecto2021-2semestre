#include <iostream>
#include <stdlib.h>
#include "Lista.h"
using namespace std;

void imprimir(Lista lNegativa, Lista lPositiva){
    cout << endl;
    cout << "\nLista Negativa." << endl;
    lNegativa.muestra();
    cout << endl;
    cout << "\nLista Positiva." << endl;
    lPositiva.muestra();
    cout << endl;
}

void Menu(int dato){
    /* Variables necesarias e instanciar las tres listas
        para la lista desordenada y la positiva y negativa ordenadas */
    string option;
    bool bandera = true;
    Lista lista = Lista();
    Lista lNegativa = Lista();
    Lista lPositiva = Lista();
    while (bandera  == true){
        cout << "\n";
        cout << "-------------------MENU-------------------" << endl;
        cout << "| [1] Insertar elementos en lista.       |" << endl;
        cout << "| [2] Mostrar lista desordenada.         |" << endl;
        cout << "| [3] Mostrar lista negativa y positiva. |" << endl;
        cout << "| [0] Salir.                             |" << endl;
        cout << "------------------------------------------" << endl << endl;
        cout << "  ¿Qué opción desea tomar: ";
        cin >> option;
        cout << endl;
        if (option == "1"){
            cout << "Ingrese dato: ";
            cin >> dato;
            system("clean");
            lista.recibir(dato);
            int aux = lista.getDato();
            /* Condiciones para saber si es negativo o positivo
                para luego ingresar en lista correspondiente junto
                a la función para ordenarlas */
            if (aux >= 0){
                lPositiva.recibirOrden(aux);
            }
            if (aux < 0){
                lNegativa.recibirOrden(aux);
            }
            cout << "\n";
        }
        else if (option == "2"){
            cout << "*Lista desordenada.*" << endl;
            lista.muestra();
            cout << "\n";
        }
        else if (option == "3"){
            imprimir(lNegativa, lPositiva);
        }
        else if (option == "0"){
            bandera = false;
        }
        else{
            cout << "Opción rechazada. Ingrese una opción correcta." << endl;
        }
    }
}

int main(){
    int dato = 0;
    Menu(dato);
    return 0;
}