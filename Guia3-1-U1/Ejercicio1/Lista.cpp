#include <iostream>
#include <stdlib.h>
#include "Lista.h"
using namespace std;

/* Constructor vacio */
Lista::Lista(){
}

/* Getter's */
int Lista::getDato(){
    return this->dato;
}

/* Métodos a utilizar */
/* Recibir el dato e insertar en lista desordenada */
void Lista::recibir(int dato){
    this->dato = dato;
    insertar(lista, this->dato);
}

/* Recibir dato e insertar en lista ordenada */
void Lista::recibirOrden(int dato){
    this->dato = dato;
    insertarOrden(lista, this->dato);
}

/* Insertar en lista desordenada */
void Lista::insertar(Nodo *&lista, int num){
    Nodo *nuevoNodo = new Nodo();
    nuevoNodo->dato = num;
    Nodo *x = lista;
    Nodo *y;
    if (lista == x){
        lista = nuevoNodo;
    }
    else{
        y->next = nuevoNodo;
    }
    nuevoNodo->next = x;
    system("clear");
    cout << "Número: " << num << ", fue añadido a la lista" << endl;
}
/* Insertar en lista ordenada */
void Lista::insertarOrden(Nodo *&lista, int num){
    Nodo *nuevoNodo = new Nodo();
    nuevoNodo->dato = num;
    Nodo *x = lista;
    Nodo *y;
    /* Método para ordenar la lista ascendetente */
    while (x != NULL && x->dato < num){
        y = x;
        x = x->next;
    }
    if (lista == x){
        lista = nuevoNodo;
    }
    else{
        y->next = nuevoNodo;
    }
    nuevoNodo->next = x;
    system("clear");
    cout << "Número: " << num << ", fue añadido a la lista" << endl;
}


/* Imprimir o mostrar lista */
void Lista::muestra(){
    Nodo *aux = new Nodo();
    /* Aux queda con primer dato */
    aux = lista;
    while (aux != NULL){
        cout << "| " << aux->dato << " |" << "" << endl;
        /* Mover dentro de lista */
        aux = aux->next;
    }
}



