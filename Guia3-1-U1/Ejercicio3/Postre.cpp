#include <iostream>
#include <stdlib.h>
#include "Postre.h"
using namespace std;

Postre::Postre(string nombre_i){
    this->nombre_i = nombre_i;
}

/* Getter's */
string Postre::getNombreI(){
    return this->nombre_i;
}

void Postre::recibir(string dato){
    this->dato = dato;
    insertarIng(this->dato);
}

void Postre::insertarIng(string dato){
    /* Reservar memoria con la creación de un nuevo nodo para luego 
        verificar si la lista o no esta vacía y asi agregar el primer elemento */
    Ingrediente *nuevoNodo = new Ingrediente();
    Ingrediente *aux;
    nuevoNodo->dato = dato;
    nuevoNodo->next = NULL;
    if (ingredientes == NULL){
        ingredientes = nuevoNodo;
    }
    else{
        aux = ingredientes;
        while(aux->next != NULL){
            aux = aux->next;
        }
        aux->next = nuevoNodo;
    }
    cout << "Ingrediente: " << dato << " , añadido a la lista de ingredientes" << endl;
}

void Postre::imprimirIng(){
    if(ingredientes != NULL){
        Ingrediente *aux = new Ingrediente();
        aux = ingredientes;
        cout << "\nIngredientes encontrados: " << endl;
        while(aux != NULL){
            cout << aux->dato << endl;
            aux = aux->next;
        }
    }
    else{
        cout << "\nNo existen ingredientes del postre." << endl;
    }
}

void Postre::eliminar(string postreEscogido){
    if(ingredientes != NULL){
        Ingrediente *aux_eliminar;
        Ingrediente *aux_anterior = NULL;
        aux_eliminar = ingredientes;
        while(aux_eliminar != NULL && aux_eliminar->dato != postreEscogido){
            aux_anterior = aux_eliminar;
            aux_eliminar = aux_eliminar->next;
        }
        /* Verificar si no existe elemento para informarle al usuario */ 
        if (aux_eliminar == NULL){
            cout << "\nIngrediente inexistente." << endl;
        }
        else if(aux_anterior == NULL){
            /* Eliminar el ingrediente de la lista con 'delete' */
            ingredientes = ingredientes->next;
            delete aux_eliminar;
            imprimirIng();
            cout << endl;
        }
        else {
            aux_anterior->next = aux_eliminar->next;
            delete aux_eliminar;
            imprimirIng();
            cout << endl;
        }
    }
    else{
        cout << "\nNo existe ingrediente para eliminar." << endl;
    }
}