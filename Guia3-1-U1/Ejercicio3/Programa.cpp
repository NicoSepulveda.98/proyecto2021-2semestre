#include <iostream>
#include <stdlib.h>
#include "Lista.h"
using namespace std;

void Menu(string dato){
    /* Variables necesarias */
    string option;
    bool bandera = true;
    /* Instanciar objeto */
    Lista l= Lista();
    while(bandera == true){
        cout << endl << endl;
        cout << "-----------MENU-----------" << endl;
        cout << "| [1] Insertar Postre    |" << endl;
        cout << "| [2] Eliminar Postre    |" << endl;
        cout << "| [3] Seleccionar Postre |" << endl;
        cout << "| [4] Mostrar Postres    |" << endl;
        cout << "| [0] Salir              |" << endl;
        cout << "--------------------------" << endl;
        cout << "\n ¿Que opción desea tomar?: ";
        cin >> option;

        if(option == "1"){
            cout << "\nIngrese nombre del postre: ";
            cin >> dato;
            l.recibir(dato);
        }
        else if(option == "2"){
            cout << "\nIngrese el postre que quiera eliminar: ";
            cin >> dato;
            l.eliminarPos(dato);
        }
        else if(option == "3"){
            l.menuIng();
        }
        else if(option == "4"){
            cout << "\nPostres: " << endl;
            l.imprimir();
        }
        else if(option == "0"){
            cout << "\n\n PROGRAMA FINALIZADO" << endl << endl;
            /* Igualar bool a false para no entrar en el ciclo */
            bandera = false;
        }
        else {
            cout << "\n\n Número incorrecto. Por favor ingrese otro valor" << endl << endl; 
        }
    }
}

int main(){
    string dato;
    Menu(dato);
    return 0;
}