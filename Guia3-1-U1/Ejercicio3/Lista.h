#include <iostream>
#include <stdlib.h>
#include "Postre.h"
using namespace std;

#ifndef LISTA_H
#define LISTA_H

struct Nodo{
    Postre *postre;
    Nodo *siguiente;
};

class Lista{
    private:
    /* Atributos */
    string dato;
    Nodo *lista = NULL;

    public:
    /* Constructor */
    Lista();
    void recibir(string dato);
    void insertar(string n);
    void imprimirPos(string postreEscogido);
    void imprimir();
    void menuIng();
    void optionUno(string postreEscogido);
    void optionDos(string postreEscogido);
    void optionTres(string postreEscogido);
    void agregarIng(string postreEscogido);
    void eliminarPos(string datoEliminar);

};


#endif