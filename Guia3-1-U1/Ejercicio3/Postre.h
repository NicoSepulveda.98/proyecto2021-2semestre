#include <iostream>
#include <stdlib.h>
using namespace std;

#ifndef POSTRE_H
#define POSTRE_H

struct Ingrediente{
    string dato;
    Ingrediente *next;
};

class Postre{
    private:
    /* Atributos */
    string dato;
    string nombre_i;
    Ingrediente *ingredientes = NULL;

    public:
    /* Constructor */
    Postre(string nombre_i);
    void recibir(string dato);
    void insertarIng(string dato);
    void imprimirIng();
    void eliminar(string postreEscogido);
    string getNombreI();
};

#endif