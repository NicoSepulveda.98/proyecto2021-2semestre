#include <iostream>
#include <stdlib.h>
#include "Postre.h"
#include "Lista.h"
using namespace std;

/* Constructor vacío */
Lista::Lista(){
}

/* Recibir dato string e incorporarlo en la lista */
void Lista::recibir(string dato){
    this->dato = dato;
    insertar(this->dato);
}

/* Insertar elementos en la lista */
void Lista::insertar(string n){
    bool bandera = false;
    Nodo *aux = new Nodo();
    aux = lista;

    while (aux != NULL && bandera == false){
        if (aux->postre->getNombreI() == n){
            bandera = true;
        }
        aux = aux->siguiente;
    }
    if (bandera == false){
        Nodo *nuevoNodo = new Nodo();
        nuevoNodo->postre = new Postre(n);
        Nodo *orden1 = lista;
        Nodo *orden2;
        /* Ordenar lista por tabla de ASCII*/
        while (orden1 != NULL && orden1->postre->getNombreI()[0] < n[0]){
            orden2 = orden1;
            orden1 = orden1->siguiente;
        }
        if (lista == orden1){
            lista = nuevoNodo;
        }
        /* Correr una posición dentro de la lista */
        else {
            orden2->siguiente = nuevoNodo;
        }
        nuevoNodo->siguiente = orden1;
        cout << "Postre: " << n << " , añadido a la lista" << endl;

    }
    else {
        cout << "\n Este postre ya existe." << endl << endl;
    }
}

/* Imprimir Postres */
void Lista::imprimirPos(string postreEscogido){
    bool bandera = false;
    Nodo *aux = new Nodo();
    aux = lista;

    while (aux != NULL && bandera == false){
        if (aux ->postre->getNombreI() == postreEscogido){
            bandera = true;
            /* Imprimir ingredientes del postre (clase Postre) */
            aux->postre->imprimirIng();
        }
        aux = aux->siguiente;
    }
    if (bandera == false){
        cout << "Postre no encontrado." << endl;
    }
}

/* Imprimir lista */
void Lista::imprimir(){
    if (lista != NULL){
        Nodo *aux = new Nodo();
        aux = lista;

        while (aux != NULL){
            cout << "Postre: " << aux->postre->getNombreI() << endl;
            aux = aux->siguiente;
        }
    }
    else {
        cout << "\nNo hay postres en la lista." << endl;
    }
}

/* Menu */
void Lista::menuIng(){
    if (lista != NULL){
        string postreEscogido = "\0";
        string option;
        cout << endl;
        cout << "-------------------MENU-------------------" << endl;
        cout << "| [1] Mostrar ingredientes de un postre. |" << endl;
        cout << "| [2] Agregar ingredientes de un postre. |" << endl;
        cout << "| [3] Borrar ingredientes de un postre.  |" << endl;
        cout << "| [0] Salir.                             |" << endl;
        cout << "------------------------------------------" << endl;
        cout << "\n ¿Que opción desea tomar?: ";
        cin >> option;
        if (option == "1"){
            system ("clear");
            optionUno(postreEscogido);
        }
        else if (option == "2"){
            system ("clear");
            optionDos(postreEscogido);
        }
        else if (option == "3"){
            system ("clear");
            optionTres(postreEscogido);

        }
        else if (option == "0"){
            system ("clear");
            cout << "\n";
        }
        else {
            cout << "Opción incorrecta." << endl << endl;
        }
    }
    else {
        cout << "\nNo hay postre para escoger." << endl << endl;
    }
}

/* Método para mostrar ingredientes a un postre */
void Lista::optionUno(string postreEscogido){
    cout << "Postres encontrados. " << endl;
    imprimir();
    cout << "\n  ¿Cúal postre desea escoger?: ";
    cin >> postreEscogido;
    imprimirPos(postreEscogido);
    cout << "\n";
    menuIng();
}

/* Método para agregar ingredentes a un postre */
void Lista::optionDos(string postreEscogido){
    cout << "Postres encontrados. " << endl;
    imprimir();
    cout << "\n  ¿Cúal postre desea agregar ingredientes?: ";
    cin >> postreEscogido;
    agregarIng(postreEscogido);
    cout << "\n";
    menuIng();
}

/* Método para eliminar ingredentes de un postre */
void Lista::optionTres(string postreEscogido){
    if (lista != NULL){
        bool bandera = false;
        Nodo *aux = new Nodo();
        aux = lista;
        string ingrediente_aux;
        cout << "Postres encontrados: " << endl;
        imprimir();
        cout << "\n  ¿Qué postre desea eliminar un ingrediente?: ";
        cin >> postreEscogido;
        while (aux != NULL && bandera == false){
            /* Condición que verifica si el postre introducido por el usuario
                es igual al nombre de la lista */
            if (aux->postre->getNombreI() == postreEscogido){
                bandera = true;
                cout << "\nIngredientes. " << endl;
                aux->postre->imprimirIng();
                cout << "  \n¿Cúal es el ingrediente que desea eliminar?, de no existir, pulse cualquier tecla: ";
                cin >> ingrediente_aux;
                /* Pasar el ingrediente la función eliminar de postre */
                aux->postre->eliminar(ingrediente_aux);
            }
            aux = aux->siguiente;
        }
        if (bandera == false){
            cout << "\nPostre no encontrado." << endl << endl;
        }   
    }
}

/* Método para agregar ingredientes */
void Lista::agregarIng(string postreEscogido){
    string ingrediente_aux;
    if (lista != NULL){
        bool bandera = false;
        Nodo *aux = new Nodo();
        aux = lista;

        while (aux != NULL && bandera == false){
            if (aux->postre->getNombreI() == postreEscogido){
                bandera = true;
                cout << "\nAgregar ingrediente: ";
                cin >> ingrediente_aux;
                aux->postre->insertarIng(ingrediente_aux);
            }
            aux = aux->siguiente;
        }
    }
    else {
        cout << "\nNo existen ingredientes." << endl << endl;
    }
}

/* Método de eliminación de postre */
void Lista::eliminarPos(string postreEscogido){
    if (lista != NULL){
        Nodo *borrar;
        Nodo *atras = NULL;
        borrar = lista;

        while (borrar != NULL && borrar->postre->getNombreI() != postreEscogido){
            atras = borrar;
            borrar = borrar->siguiente;
        }
        if (borrar == NULL){
            cout << "No hay postre." << endl << endl;
        }
        else if (atras == NULL){
            lista = lista->siguiente;
            delete borrar;
            cout << "\nPostres encontrados: " << endl;
            imprimir();
            cout << "\n";
        }
        else {
            atras->siguiente = borrar->siguiente;
            delete borrar;
            cout << "\nPostres encontrados: " << endl;
            imprimir();
            cout << "\n";
        }
    }
    else {
        cout << "\nNo existe ese postre." << endl;
    }
}