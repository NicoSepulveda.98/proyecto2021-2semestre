#include <iostream>
#include <stdlib.h>
#include "Lista.h"
#include <string.h>

using namespace std;

/* Se intento crear una función para verificar mediante char
    las minus-mayus de los nombres junto con entrega solo de palabras
    y no números.
int validar(string dato_aux){
    char c;
    for( int i = 0; i < dato_aux.size(); i++){
        c = dato_aux[i];
        if(isalpha(c) == 0 && isspace(c) == 0){
            cout << "Dato ingresado incorrecto. Ingrese nuevamente." << endl;
            return 0;
        }
        else{
            cout << "Dato " << "| " << dato_aux << " |" << " ingresado correctamente" << endl;
        
        }
    }
    return 1;
}
*/
void Menu(string dato){
    /* Variables necesarias */
    string option;
    bool bandera = true;
    /* Instanciar objeto */
    Lista lista = Lista();
    while(bandera == true){
        cout << endl;
        cout << "-----------MENU-----------" << endl;
        cout << "| [1] Insertar nombres   |" << endl;
        cout << "| [0] Salir              |" << endl;
        cout << "--------------------------" << endl;
        cout << "\n ¿Que opción desea tomar?: ";
        cin >> option;

        if(option == "1"){
            cout << "\nPalabra a digitar (los nombres en minúscula quedarán bajo la lista): ";
            cin >> dato;
            lista.recibir(dato);

        }
        else if(option == "0"){
            cout << "\n\n PROGRAMA FINALIZADO" << endl << endl;
            /* Igualar bool a falso para no entrar en el ciclo */
            bandera = false;
        }
        else {
            cout << "\n\n Número incorrecto. Por favor ingrese otro valor" << endl << endl; 
        }
    }
}

int main(){
    string dato;
    Menu(dato);
    return 0;
}
