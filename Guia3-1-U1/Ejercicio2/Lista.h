#include <iostream>
#include <stdlib.h>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

/* Crear Nodo */
struct Nodo{
    string dato;
    Nodo *next;
};

class Lista{
    /* Atributos */
    private:
    string dato;
    Nodo *lista = NULL;

    /* Constructor */
    public:
    Lista();
    void recibir(string dato);
    void imprimir(Nodo *lista);
    void insertar(Nodo *&lista, string n);

};

#endif