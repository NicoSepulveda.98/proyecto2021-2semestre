#include <iostream>
#include <stdlib.h>
#include "Lista.h"
using namespace std;

/* Constructor vacio */
Lista::Lista(){
}

/* Métodos a utilizar */
/* Recibir el dato e insertar en lista */
void Lista::recibir(string dato){
    this->dato = dato;
    insertar(lista, this->dato);
}

/* Insertar en lista */
void Lista::insertar(Nodo *&lista, string n){
    Nodo *nuevoNodo = new Nodo();
    nuevoNodo->dato = n;
    Nodo *x = lista;
    Nodo *y;
    while((x != NULL) && (x->dato < n)){
        y = x;
        x = y->next;
    }
    if (lista == x){
        lista = nuevoNodo;
    }
    else {
        y->next = nuevoNodo;
    }
    nuevoNodo->next = x;
    system("clear");
    cout << "El ingreso de " << "> " << n << " <" << " fue añadido a la lista" << endl;
    imprimir(lista);
}

/* Imprimir o mostrar lista */
void Lista::imprimir(Nodo *lista){
    Nodo *aux = new Nodo();
    /* aux queda con primer dato */
    aux = lista;
    cout << "\nLista: " << endl << endl;
    while (aux != NULL){
        cout << "| " << aux->dato << " |" << "" << endl;
        /* Mover dentro de lista al dato siguiente */
        aux = aux->next;
    }
}