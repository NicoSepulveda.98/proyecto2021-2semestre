#include <iostream>
#include <stdlib.h>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

struct Nodo {
    int dato;
    Nodo *next;
};

class Lista {
    private:
    int dato;
    Nodo *lista = NULL;
    int mayor = 0;
    int menor;
    
    public:
    /* Constructor */
    Lista();
    void recibir(int dato);
    void insertar(Nodo *&lista, int n);
    void muestra();
    void calcularMayorMenor();
    void eliminarvaciar();
    void ejecturarEliminar();
    int getmayor();
    int getmenor();
};

#endif