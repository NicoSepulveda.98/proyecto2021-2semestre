#include <iostream>
#include <stdlib.h>
#include "Lista.h"
using namespace std;

void RellenarOrdenar(Lista lista, int dato ){
    /* Obtener numero mayor y menor */
    int mayor = lista.getmayor();
    int menor = lista.getmenor();
    lista.ejecturarEliminar();
    /* Se rellena la lista del numero mayor hasta el menor */
    for (int i = menor; i <= mayor; i++ ){
        lista.recibir(i);
    }
    /* Mostrar la lista rellenada */
    cout << "\nLista rellenada y ordenada" << endl;
    lista.muestra();
}

void Menu(int dato){
    /* Variables utilizables */
    string option;
    bool bandera = true;
    Lista l = Lista();
    while (bandera == true){
        cout << endl;
        cout << "[1] Insertar elementos" << endl;
        cout << "[2] Rellenar/Ordenar lista" << endl;
        cout << "[0] Salir" << endl;
        cout << "¿Qué opción desea?: ";
        cin >> option;
        
        if (option == "1"){
            cout << "Ingrese el número: ";
            cin >> dato;
            l.recibir(dato);
        }
        else if (option == "2"){
            l.muestra();
            l.calcularMayorMenor();
            RellenarOrdenar(l, dato);
        }
        else if (option == "0"){
            cout << "\n PROGRAMA FINALIZADO" << endl;
            bandera = false;
        }
        else {
            system("clear");
            cout << "Opción invalida. Ingrese nuevamente" << endl;
        }
    }
}

int main(){
    int dato = 0;
    Menu(dato);
    return 0;
}