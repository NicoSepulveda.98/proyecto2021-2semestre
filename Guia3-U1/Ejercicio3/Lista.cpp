#include <iostream>
#include <stdlib.h>
#include "Lista.h"
using namespace std;

/* Constructor vacío */
Lista::Lista(){
}

/* Getter's */
int Lista::getmayor(){
    return this->mayor;
}

int Lista::getmenor(){
    return this->menor;
}

/* Recibir dato para la lista */
void Lista::recibir(int dato){
    this->dato = dato;
    insertar(lista, this->dato);
}

/* Insertar elementos a la lista (nuevo Nodo) */
void Lista::insertar(Nodo *&lista, int n){
    Nodo *nuevoNodo = new Nodo();
    nuevoNodo->dato = n;
    Nodo *x = lista;
    Nodo *y;
    
    while(x != NULL && x->dato < n){
        y = x;
        x = x->next;
    }
    if (lista == x){
        lista = nuevoNodo;
    }
    else {
        y->next = nuevoNodo;
    }
    nuevoNodo->next = x;
    system("clear");
    cout << "Número " << "> " << n << " <" << " se agregó a la lista" << endl;
}

/* Mostrar lista */
void Lista::muestra(){
    Nodo *aux = new Nodo();
    aux = lista;
    cout << "*Lista Ordenada*" << endl;
    while (aux != NULL){
        cout << "[ " << aux->dato << " ]" << "" << endl;
        aux = aux->next;
    }
}

/* Calcular dato mayor y menor de la lista */
void Lista::calcularMayorMenor(){
    while (lista != NULL){
        if (lista->dato > mayor){
            this->mayor = lista->dato;
        }
        if (lista->dato < menor){
            this->menor = lista->dato;
        }
        /* Avanza una posición en la lista */
        lista = lista->next; 
    }
}

/* Eliminar o vaciar lista */
void Lista::eliminarvaciar(){
    Nodo *aux = lista;
    lista = aux->next;
    delete aux;
}

/* Ejecutar el método de eliminar o vaciar */
void Lista::ejecturarEliminar(){
    while (lista != NULL){
        eliminarvaciar();
    }
}