#include <iostream>
#include <stdlib.h>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

struct Nodo{
    int dato;
    Nodo *next;
};

class Lista{
    private:
    int dato;
    Nodo *lista = NULL;

    public:
    /* Constructor */
    Lista();
    void recibir(int dato);
    void muestra(Nodo *lista);
    void insertar(Nodo *&lista, int n);

};

#endif