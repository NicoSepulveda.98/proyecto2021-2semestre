#include <iostream>
#include <stdlib.h>
#include "Lista.h"
using namespace std;

/* Constructor vacío */
Lista::Lista(){
}

/* Recibir dato */
void Lista::recibir(int dato){
    this->dato = dato;
    /* Insertar dato en lista */
    insertar(lista, this->dato);
}

/* Insertar elementos a lista */
void Lista::insertar(Nodo *&lista, int n){
    /* Instanciar memoria del nuevo Nodo */
    Nodo *nuevoNodo = new Nodo();
    nuevoNodo->dato = n;
    Nodo *x = lista;
    Nodo *y;
    /*"Lista ordenada de menor a mayor */
    while(x != NULL && x->dato < n){
        y = x;
        x = x->next;
    }
    if(lista == x){
        lista = nuevoNodo;
    }
    else{
        y->next = nuevoNodo;
    }
    nuevoNodo->next = x;
    system("clear");
    cout << "Número " << "> " << n << " <" << " esta añadido a la lista" << endl;
    muestra(lista);
}

/* Mostrar lista */
void Lista::muestra(Nodo *lista){
    Nodo *aux_nodo = new Nodo();
    aux_nodo = lista;
    cout << "\nLista Ordenada" << endl;
    while(aux_nodo != NULL){
        cout << "[ " << aux_nodo->dato << " ]" << "" << endl;
        aux_nodo = aux_nodo->next;
    }
}

