#include <iostream>
#include <stdlib.h>
#include "Lista.h"
using namespace std;

void Menu(int dato){
    /* Variables a utilizar */
    string option;
    bool bandera = true;
    /* Instanciar lista */
    Lista l = Lista();
    while (bandera == true){
        cout << endl;
        cout << "[1] Insertar elemento" << endl;
        cout << "[0] Salir" << endl;
        cout << "¿Qué opción toma?: ";
        cin >> option;
        if(option == "1"){
            cout << "Ingrese el número: ";
            cin >> dato;
            cout << endl;
            l.recibir(dato);
        }
        else if(option == "0"){
            cout << "\nPROGRAMA FINALIZADO" << endl;
            bandera = false;
        }
        else{
            system("clear");
            cout << "\nNúmero incorrecto. Por favor ingrese otro valor" << endl;
        }
    }
}

int main(){
    int dato = 0;
    Menu(dato);
    return 0;
}

