#include <iostream>
#include <stdlib.h>
#include "Lista.h"
using namespace std;

/* Constructor vacío */
Lista::Lista(){
}

/* Getter's */
int Lista::getdato(){
    return this->dato;
}

/* Recibir dato */
void Lista::recibir(int dato){
    this->dato = dato;
    insertar(lista, this->dato);
}

 /* Insertar elementos a la lista */
void Lista::insertar(Nodo *&lista, int n){
    Nodo *nuevoNodo = new Nodo();
    nuevoNodo->dato = n;
    Nodo *x = lista;
    Nodo *y;
    
    while(x != NULL && x->dato < n){
        y = x;
        x = x->next;
    }

    if (lista == x){
        lista = nuevoNodo;
    }
    else {
        y->next = nuevoNodo;
    }
    nuevoNodo->next = x;
}

/* Mostrar lista */
void Lista::muestra(){
    Nodo *aux = new Nodo();
    aux = this->lista;
    cout << "\nLista Ordenada" << endl;
    while (aux != NULL){
        cout << "[ " << aux->dato << " ]" << "" << endl;
        aux = aux->next;
    }
}