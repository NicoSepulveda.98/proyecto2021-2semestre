#include <iostream>
#include <stdlib.h>
#include "Lista.h"

using namespace std;

void Menu(int dato){
    /* Variables a utilizar */
    int aux;
    string option;
    bool bandera = true;
    /* Instanciar las tres listas */
    Lista l1 = Lista();
    Lista l2 = Lista();
    Lista l3 = Lista();

    while (bandera == true){
        cout << endl;
        cout << "[1] Insertar elemento en lista 1" << endl;
        cout << "[2] Insertar elemento en lista 2" << endl;
        cout << "[3] Mostrar lista 3 (almacén lista 1 y lista 2)" << endl;
        cout << "[0] Salir" << endl;
        cout << "¿Qué opción desea?: ";
        cin >> option;
        cout << endl;
        if (option == "1"){
            cout << "Digite un número: ";
            cin >> dato;
            l1.recibir(dato);
            system("clear");
            cout << "\nNúmero " << "> " << l1.getdato() << " <" << " añadido a la lista 1" << endl;
            l1.muestra();
            aux = l1.getdato();
            /* Se añade el dato a la lista 3 */
            l3.recibir(aux);
        }
        else if (option == "2"){
            cout << "Digite un número: ";
            cin >> dato;
            l2.recibir(dato);
            system("clear");
            cout << "\nNúmero " << "> " << l2.getdato() << " <" << " añadido a la lista 2" << endl;
            l2.muestra();
            aux = l2.getdato();
            /* Se añade el dato a la lista 3 */
            l3.recibir(aux);
        }
        else if (option == "3"){
            cout << "Lista 3 -> Conjunto de Lista 1 & Lista 2" << endl;
            l3.muestra();
        }
        else if (option == "0"){
            cout << "\n PROGRAMA FINALIZADO" << endl;
            bandera = false;
        }
        else {
            system("clear");
            cout << "Opción incorrecta, ingrese otro valor" << endl;
        }
    }
}

int main (){
    int dato = 0;
    Menu(dato);
    return 0;
}