#ifndef PRIM_H
#define PRIM_H

#include <iostream>
#include <fstream>

using namespace std;

class Prim{
    private:
    /* Atributos privados */
    ofstream files;
    int **matriz;
    char nodoID;
    int dis;

    public:
    /* Constructor */
    Prim();
    void agregar_nombres(int total_nodos, char nombres[], int **matriz);
    void rellenar_matriz(int total_nodos, char nombres[], int **matriz);
    void imprimir_matriz(int **matriz, int n);
    void inicializar_vector_caracter(char *vector, int n);
    void imprimir_vector_caracter(char *vector, int n);
    void inicializar_matriz_enteros (int **matriz, int n);
    int min(int *valor_clave, bool *mstSet, int n);
    void imprimirMST(int *pariente, int **matriz, int n, char *vector);
    void primMST(int **matriz, int n, char *vector);
    void imprimir_grafo(int **matriz, char *vector, int n);
    void generargrafoinicial(int **matriz, char *vector, int n);
    void generargrafofinal(int **matriz, int *pariente, int *valor_clave, int n, char *vector);

};

#endif