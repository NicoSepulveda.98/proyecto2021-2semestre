graph G {
graph [rankdir = LR]
node [style=filled fillcolor=gray]

A -- B [label=1];
A -- C [label=3];
B -- C [label=3];
B -- D [label=6];
C -- D [label=4];
C -- E [label=2];
D -- E [label=5];
}
