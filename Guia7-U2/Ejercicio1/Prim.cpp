#include <iostream>
#include <climits>
#include "Prim.h"
#include <fstream>
using namespace std;

/* Constructor */
Prim::Prim(){

}
/* Agregar nombres de los Nodos*/
void Prim::agregar_nombres(int total_nodos, char nombres[], int **matriz){
    int contador = 0;
    while (contador < total_nodos){
        cout << "Ingrese nombre de nodo " << contador+1 << ": ";
        cin >> nodoID;
        nombres[contador] = nodoID;
        contador++;
    }
}

/* Se rellena la matríz */
void Prim::rellenar_matriz(int total_nodos, char nombres[], int **matriz) {
    int i = 0;
    int j = 0;
    for (i = 0; i < total_nodos; i++){  
        cout << "\nTrabajando con nodo: " << nombres[i] << endl;
        for (j = 0; j < total_nodos; j++){
            if (i != j && matriz[i][j] != 0){ 
                cout << " > Digite la distancia entre nodos " << nombres[i] << " hasta " << nombres[j] << ": ";
                cin >> dis;
                matriz[i][j] = dis;
                //matriz[j][i] = 0;
            }
            else {
                matriz[i][j] = 0;
            }
        }
    }
}
/* Se imprime matriz */
void Prim::imprimir_matriz(int **matriz, int n) {
    cout << endl;
    cout << "--> Matriz Impresa <-- " << endl << endl;
    for (int fila=0; fila<n; fila++) {
        for (int col=0; col<n; col++) {
            cout << matriz[fila][col] << "  ";
        }
        cout << endl;
    }
}

// Inicializa matriz nxn. Recibe puntero a la matriz.
void Prim::inicializar_matriz_enteros(int **matriz, int n) {
    for (int fila=0; fila<n; fila++) {
        for (int col=0; col<n; col++) {
            matriz[fila][col] = -1;
        }
    }
}
/* Función para encontrar el vértice con valor de clave mínimo */ 
int Prim::min(int *valor_clave, bool *mstSet, int n){
    /* Se crea una variable valor entero para igualarlo a un predeterminado
    infinito y variable a utilizar */
    int minimo = INT_MAX, minimo_index;
    for (int i = 0; i < n; i++){
        if (mstSet[i] == false && valor_clave[i] < minimo){
            minimo = valor_clave[i], minimo_index = i;
        }
    }
    return minimo_index;
}

/* Imprimir el MST(Prim) almacenado en *pariente (esto es tipo padre) */
void Prim::imprimirMST(int *pariente, int **matriz, int n, char *vector){
    int peso_total = 0;
    cout << "\nBorde \tPeso\n" << endl;
    for (int i = 1; i < n; i++){
        cout << vector[pariente[i]] << " - " << vector[i] << endl;
        cout << pariente[i] << " - " << i << " \t" << matriz[i][pariente[i]] << " \n" << endl;
        peso_total = peso_total + matriz[i][pariente[i]];
    }
    cout << " \n Peso total es: " << peso_total << endl;
}

/* Función de construcción e impresión de MST(Prim) y gráfico representando por matriz de adyacencia */
void Prim::primMST(int **matriz, int n, char *vector){
    /* Variable de almacenamiento del MST */
    int pariente[n];
    /* Valores claves para elegir el borde de peso */
    int valor_clave[n];
    /* Booleano para representar si el conjunto de vértices están incluidos en MST(Prim) */
    bool mstSet[n];
    for (int i = 0; i < n; i++){
        /* Inicializar todos los valores clave de peso a infinito */
        valor_clave[i] = INT_MAX, mstSet[i] = false;
    }
    /* Incluir siempre el primer vértice en MST y establecer el valor_clave a 0 para
    que ese vértice se elija como primero */
    valor_clave[0] = 0;
    /* Establecer primer nodo como ráiz */
    pariente[0] = -1;
    for (int count = 0; count < n -1; count ++){
        /* Elegir el vértice mínimo aún no incluido en MST */
        int u = min(valor_clave, mstSet, n);
        /* Luego se agrega el vértice al MST */
        mstSet[u] = true;
        /* Actualizar vértice de valor clave y el índice principal de los vértices.
        No se consideran los vértices que estan incluidos en el MST */
        for (int j = 0; j < n; j++){
            if (matriz[u][j] && mstSet[j] == false && matriz[u][j] < valor_clave[j]){
                pariente[j] = u, valor_clave[j] = matriz[u][j];
            }
        }
    }
    /* Imprimir el MST construido */
    imprimirMST(pariente, matriz, n, vector);
    generargrafofinal(matriz, pariente, valor_clave, n, vector);
}

// Método que genera grafo de la estructura.
void Prim::generargrafoinicial(int **matriz, char *vector, int n) {
    ofstream files;

    files.open("grafo.txt", ios::out);
    files << "graph G {" << endl;
    files << "graph [rankdir = LR]" << endl;
    files << "node [style=filled fillcolor=gray]" << endl << endl;

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++){
            if (i != j && i < j && matriz[i][j]) {
                files << vector[i] << " -- " << vector[j] << " ";
                files << "[label=" + to_string(matriz[i][j]) << "];" << endl;
            }
        }
    }

    files << "}" << endl;
    files.close();

    system("dot -Tpng -ografo.png grafo.txt &");
    system("eog grafo.png &");
}


// Método que genera grafo de la estructura.
void Prim::generargrafofinal(int **matriz, int *pariente, int *valor_clave, int n, char *vector) {
	ofstream files;

	files.open("grafoFinal.txt", ios::out);
	files << "graph G {" << endl;
	files << "graph [rankdir = LR]" << endl;
	files << "node [style=filled fillcolor=gray]" << endl << endl;

	// recorrerGrafoFinal(vector, files);		//! DEPRECATED

	for(int i = 1; i < n; i++) {
		files << vector[pariente[i]] << " -- " << vector[i] << " ";
		files << "[label=" + to_string(valor_clave[i]) << "];" << endl;
	}

	files << "}" << endl;
	files.close();

	system("dot -Tpng -ografoFinal.png grafoFinal.txt &");
	system("eog grafoFinal.png &");
}