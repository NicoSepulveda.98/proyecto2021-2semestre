#include <iostream>
#include "Prim.h"
using namespace std;

int main(int argc, char *argv[]){
    int n;
    n = atoi(argv[1]);
    if (n <= 2 || n >= 100){
        cout << "Valor invalido." << endl;
        return -1;
    }
    else {
        char V[n];
        int **matriz_aux;
        matriz_aux = new int*[n];
        for (int i = 0; i < n; i++){
            matriz_aux[i] = new int[n];
        }
        Prim prim;
        int tecla = 0;
        string tecla_op = "\0";
        int bandera = true;
        
        do {
            cout << endl;
            cout << "|------------- MENU -------------|" << endl;
            cout << "| [1] Ingreso y muestra de datos.|" << endl;
            cout << "| [2] Muestra grafo.             |" << endl;
            cout << "| [3] Salir.                     |" << endl;
            cout << "|--------------------------------|" << endl;
            cout << " > ¿Qué opción toma?: ";
            cin >> tecla_op;
            tecla = atoi(tecla_op.c_str());
            cin.ignore();

            if (tecla == 1){
                prim.inicializar_matriz_enteros(matriz_aux, n); 
                prim.agregar_nombres(n, V, matriz_aux);
                prim.rellenar_matriz(n, V, matriz_aux);
                prim.imprimir_matriz(matriz_aux, n);
                prim.generargrafoinicial(matriz_aux, V, n);
                prim.primMST(matriz_aux, n, V);
            }
            else if (tecla == 2){
                prim.generargrafoinicial(matriz_aux, V, n);
                prim.primMST(matriz_aux, n, V);
            }
            else if (tecla == 3){
                bandera = false;
            }
            else {
                cout << "\nTecla incorrecta." << endl;
            }
        }while(bandera == true);
    }
    return 0;
}