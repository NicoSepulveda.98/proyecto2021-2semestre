#include <iostream>
#include <stdlib.h>
using namespace std;
#ifndef PILA_H
#define PILA_H

class Pila{
    private:
    /* Atributos privados */
    int max = 0;
    int *pila = NULL;
    int tope = 0;
    bool band = true;

    public:
    /* Constructor */
    Pila (int max);
    void push();
    void pop();
    void ver();
    void pilaVacia();
    void pilaLlena();
};

#endif