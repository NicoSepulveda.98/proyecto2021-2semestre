#include <iostream>
#include <stdlib.h>
#include "Pila.h"
using namespace std;

int main(){
    /* Instanciar variables a utilizar y el menu de opciones */
    string option;
    int aux_max;
    cout << "Ingrese el máximo de la Pila: ";
    cin >> aux_max;
    /* Pasa como parámetro el valor del usuario para saber el max de la pila */
    Pila pila = Pila(aux_max);
    cout << "\n";
    bool bandera = true;
    while (bandera){
        cout << "----------------" << endl;
        cout << "Agregar/push [1]" << endl;
        cout << "Remover/pop  [2]" << endl;
        cout << "Ver pila     [3]" << endl;
        cout << "Salir        [0]" << endl;
        cout << "----------------" << endl;
        cout << "Presione la opción: ";
        cin >> option;
        system("clear");

        if (option == "1"){
            system("clear");
            pila.push();
        }
        else if (option == "2"){
            system("clear");
            pila.pop();
        }
        else if (option == "3"){
            system("clear");
            pila.ver();
        }
        else if (option == "0"){
            system("clear");
            exit(0);
        }
    }
    return 0;

}