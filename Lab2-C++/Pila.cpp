#include <iostream>
#include <stdlib.h>
#include "Pila.h"
using namespace std;

/* Constructor*/
Pila::Pila(int max){
    this->max = max;
    cout << "\nEl tamaño máximo de la pila es: " << max;
    this->pila = new int[this->max];
    
}

/* Verificar si la pila esta vacía */
void Pila::pilaVacia(){
    if (tope == 0){
        band = true;
    }
    else {
        band = false;
    }
}

/* Verificar si la pila esta llena */
void Pila::pilaLlena(){
    if (tope == max){
        band = true;
    }
    else {
        band = false;
    }
}

/* Agregar los valores a la pila */
void Pila::push(){
    pilaLlena();
    if (band == true){
        cout << "<< Desbordamiento, pila llena >>" << endl << endl;
    }
    else {
        int dato;
        for (int i = 0; i < max; i++){
            cout << "Ingrese: ";
            cin >> dato;
            tope = tope +1;
            pila[tope] = dato;
            cout << "\n";
        }
    }
}

/* Eliminar valores de la pila */
void Pila::pop(){
    int dato;
    pilaVacia();
    if (band == true){
        cout << ">> Subdesbordamiento, pila vacía <<" << endl << endl;
    }
    else {
        dato = pila[tope];
        tope = tope -1;
        cout << "Dato eliminado: " << dato;
        cout << "\n";
    }
}

/* Mostrar pila */
void Pila::ver(){
    cout << "----------------" << endl;
    cout << ">> PILA <<" << endl;
    /* For decreciente, ya que al convertir el ';i--' en ';i++' ocurre desbordamiento de
    buffer (ciclo de 0) */
    for (int i = tope; i > 0; i--){
        cout << "|" << pila[i] << "|" << endl;
    }
    cout << "----------------" << endl;
}