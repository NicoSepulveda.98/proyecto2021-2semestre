#ifndef ATOMO_H
#define ATOMO_H
#include "Coordenada.h"
#include <string>
using namespace std;

class Atomo{
    private:
    string nombre;
    int numero;
    Coordenada coordenada;

    public:
    Atomo(string nombre, int numero);

    string get_nombre();
    int get_numero();
    Coordenada get_coordenada();
    void set_nombre(string nombre);
    void set_numero(int numero);
    void set_coordenada(Coordenada coordenada);

};
#endif 