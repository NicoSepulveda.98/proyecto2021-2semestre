**Almacenador de Proteínas**

El siguiente programa consiste en un programa que va orientado a un pequeño código que deja almacenar una proteína, junto con otros datos necesarios.
Por ejemplo, se almacena una cadena junto a los aminoacidos que posee y los atomos con sus cantidades.

**Historia:**

Fue escrito y desarrollado por Nicolás Sepúlveda Falcón, utilizando para ello el lenguaje de programación C++. Se trata de un lenguaje robusto fuertemente equipado y que sigue un paradigma de programación orientada a objetos.

**Para empezar:**

Es requisito tener instalado **C++**, de preferencia compilador **g++ 9.3.0**, previamente en su computadora, de lo contrario, el programa no podrá ser lanzado.
Recomendamos que el sistema operativo sobre el cual pretende lanzar el programa, corra sobre el kernel Linux (Debian, Ubuntu, Arch, entre otras.)

**Instalación:**

Para instalar y ejecutar el programa en su máquina, es necesario que siga las presentes indicaciones:
1- Clonar el repositorio "proyecto2021-2semestre" en el directorio de su preferencia, el enlace HTTPS para clonar el programa ingresando a la terminal es el siguiente:
```
git clone https://gitlab.com/NicoSepulveda.98/proyecto2021-2semestre.git 
```
2- Luego, deberá entrar en la carpeta clonada que lleva por nombre "proyecto2021-2semestre" especificamente en "G0-C++" que contiene las clases en formato ".cpp" y ".h"
3- Posteriormente, ya está usted preparado para ejecutar el programa. Puede ser lanzado desde una consola de su IDE de preferencia o desde una terminal en Linux

**Codificación:**

El programa soporta la codificación estándar UTF-8

**Construido con:**

Visual Studio Code, IDE utilizado por defecto para el desarrollo del proyecto.
Sitio web para descargar para Linux, Windows y Mac en https://code.visualstudio.com/download"

**Licencia:**

Este proyecto está sujeto bajo la Licencia GNU GPL v3.

**Autores y creadores del proyecto:**

> Nicolás Sepúlveda Falcón.

