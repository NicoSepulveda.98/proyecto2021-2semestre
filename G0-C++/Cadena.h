#ifndef CADENA_H
#define CADENA_H
#include "Aminoacido.h"
#include <string>
using namespace std;
#include <list>

class Cadena{
    private: 
    string letra;
    list<Aminoacido> aminoacidos;

    public:
    Cadena();
    Cadena(string letra);

    string get_letra();
    list<Aminoacido> get_aminoacido();
    void set_letra(string letra);
    void add_aminoacido(Aminoacido aminoacido);
};

#endif 