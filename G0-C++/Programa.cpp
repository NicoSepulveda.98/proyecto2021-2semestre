/*

g++ Programa.cpp Proteina.cpp Cadena.cpp Aminoacido.cpp Atomo.cpp Coordenada.cpp -o programa 
./programa

*/

// Incluir librerías y clases a utilizar
#include <iostream>
#include <stdlib.h>
#include <cstdlib>
#include <list>
#include "Proteina.h"
#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"
using namespace std;


void imprimir_datos(list<Proteina> proteinas){
    system("clear");
    cout << "\tPROTEÍNAS ALMACENADAS " << endl << endl;
    // Ciclos para utilizar métodos de obtención y actualización de difentes clases (setter's & getter's)
    for(Proteina p: proteinas){
        cout << "--> Nombre proteína: " << p.get_nombre() << endl;
        cout << "--> ID: " << p.get_id() << endl;
        for(Cadena c: p.get_cadena()){
            cout << "--> La letra de la cadena es: " << c.get_letra() << endl;
            for(Aminoacido am: c.get_aminoacido()){
                cout << "--> El aminoácido -> " << am.get_nombre() << " <- posee un número de: " << am.get_numero() << endl;
                for(Atomo at: am.get_atomos()){
                    cout << "--> Posee " << at.get_numero() << " átomos de: " << at.get_nombre() << endl;
                    cout << "--> Sus coordenadas son:" << endl;
                    cout << "       *Coordenada x -> " << at.get_coordenada().get_x();
                    cout << "       *Coordenada y -> " << at.get_coordenada().get_y();
                    cout << "       *Coordenada z -> " << at.get_coordenada().get_z() << endl << endl;
                }
            }
        }
    }
}

int leer_datos_proteina(){
    // Variables a utilizar
    list<Proteina> proteinas;
    string teclaStr;
    int tecla;
    int numero_veces = 0;
    string nombre_proteina;
    string aux_id;
    string nombre_cadena;
    string nombre_aminoacidos;
    int numero_aminoacidos;
    string nombre_atomos;
    int numero_atomos;
    float x;
    float y;
    float z;

    while(true){
        cout << "¡ESCOJA LA OPCIÓN QUE DESEA EJECUTAR!" << endl << endl;
        cout << ">> [1] Crear lista con datos de la proteína" << endl;
        cout << ">> [2] Imprimir proteína" << endl;
        cout << ">> [3] Salir del programa" << endl << endl;
        cout << "¿Qué opción desea hacer?: ";
        // Recuperar variable en caso de ingresar un String
        cin >> teclaStr;
        tecla = atoi(teclaStr.c_str());
        cout << endl;
        if(tecla == 1){
            cout << "Ingrese nombre proteína: ";
            cin >> nombre_proteina;
            cout << "Ingrese ID de la proteína: ";
            cin >> aux_id;
            // Guardar datos de proteína en su constructor por parámetros
            Proteina prot = Proteina(nombre_proteina, aux_id);
            cout << "¿Cuántas cadenas desea ingresar?: ";
            cin >> numero_veces;
            for (int i = 0; i < numero_veces; i++){
                cout << "Ingrese la letra de la cadena " << i+1 << ": ";
                cin >> nombre_cadena;
            }
            // Guardar datos de proteína/cadena en su constructor por parámetros
            Cadena cadena = Cadena(nombre_cadena);
            numero_veces = 0;
            cout << "¿Cuántos aminoácidos desea ingresar?: ";
            cin >> numero_veces;
            for (int i = 0; i < numero_veces; i++){
                cout << "Ingrese nombre del aminoácido: ";
                cin >> nombre_aminoacidos;
                cout << "Ingrese numero del aminoácido: ";
                cin >> numero_aminoacidos;
            }
            // Guardar datos de proteína/aminoácido en su constructor por parámetros
            Aminoacido amino = Aminoacido(nombre_aminoacidos, numero_aminoacidos);
            numero_veces = 0;
            cout << "¿Cuántos átomos desea ingresar?: ";
            cin >> numero_veces;
            for (int i = 0; i < numero_veces; i++){
                cout << "Ingrese nombre del átomo: ";
                cin >> nombre_atomos;
                cout << "Ingrese el número de átomos: ";
                cin >> numero_atomos;
                cout << "Ingrese coordenada X: ";
                cin >> x;
                cout << "Ingrese coordenada Y: ";
                cin >> y;
                cout << "Ingrese coordenada Z: ";
                cin >> z;
            }
            // Guardar datos de proteína/átomo/coordenada en su constructor por parámetros
            Coordenada coor = Coordenada(x,y,z);
            Atomo atomo = Atomo(nombre_atomos, numero_atomos);
            // Guardado de datos en los setter's y listas correspondientes
            atomo.set_coordenada(coor);
            amino.add_atomos(atomo);
            cadena.add_aminoacido(amino);
            prot.add_cadena(cadena);
            proteinas.push_back(prot);
        }
        else if(tecla == 2){
            imprimir_datos(proteinas);
        }
        else if(tecla == 3){
            break;
        }
        else{
            cout << "Tecla inválida, por favor inténtelo nuevamente" << endl;
        }
    }
    return tecla; 
}
// Función Main()
int main(int argc, char **argv){
    leer_datos_proteina();

    return 0;

}