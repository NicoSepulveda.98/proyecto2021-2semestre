#ifndef PROTEINA_H
#define PROTEINA_H
#include "Cadena.h"
#include <string>
using namespace std;
#include <list>

class Proteina{
    private:
    string nombre;
    string id;
    list<Cadena> cadenas;

    public:
    Proteina ();
    Proteina(string nombre, string id);

    string get_nombre();
    string get_id();
    list<Cadena> get_cadena();
    void set_nombre(string nombre);
    void set_id(string id);
    void add_cadena(Cadena cadena);
    
};

#endif 