#include <iostream>
using namespace std;
#include "Proteina.h"

Proteina::Proteina(){
    string nombre;
    string id;
    list<Cadena> cadenas;
}

Proteina::Proteina(string nombre, string id){
    this-> nombre = nombre;
    this-> id = id;
}

string Proteina::get_nombre(){
    return this-> nombre;
}

string Proteina::get_id(){
    return this-> id;
}

list<Cadena> Proteina::get_cadena(){
    return this->cadenas;
}

void Proteina::set_nombre(string nombre){
    this-> nombre = nombre;
}

void Proteina::set_id(string id){
    this-> id = id;
}

void Proteina::add_cadena(Cadena cadena){
    this-> cadenas.push_back(cadena);
}