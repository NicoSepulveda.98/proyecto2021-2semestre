#ifndef AMINOACIDO_H
#define AMINOACIDO_H
#include "Atomo.h"
#include <string>
using namespace std;
#include <list>

class Aminoacido{
    private:
    string nombre;
    int numero;
    list<Atomo> atomos;

    public:
    Aminoacido();
    Aminoacido(string nombre, int numero);
    string get_nombre();
    int get_numero();
    list<Atomo> get_atomos();
    void set_nombre(string nombre);
    void set_numero(int numero);
    void add_atomos(Atomo atomo);
};

#endif 