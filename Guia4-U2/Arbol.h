#include <iostream>
#include <stdlib.h>
#include <unistd.h>
using namespace std;
#ifndef ARBOL_H
#define ARBOL_H

/* Se crea la estructura del Nodo */
typedef struct _Nodo{
    int i;
    struct _Nodo *izq;
    struct _Nodo *der;
    struct _Nodo *padre;
} Nodo;

class Arbol {
    /* Atributo privados vacío */
    private:

    public:
    /* Constructor y métodos a utilizar */
    Arbol();
    /* Funciones de manipulación del árbol */
    void crearGrafo(Nodo *raiz);
    void recorrerArbol(Nodo *nodo, ofstream &arch);
    void insertarNodo(Nodo *&arbol, int n, Nodo *padre);
    bool busqueda(Nodo *arbol, int n);
    /* Funciones para los tres ordenes del árbol */
    void preOrden(Nodo *arbol);
    void inOrden(Nodo *arbol);
    void posOrden(Nodo *arbol);
    /* Funciones de eliminación del nodo */
    void destruirNodo(Nodo *nodo);
    void reemplazarNodo(Nodo *arbol, Nodo *nuevoNodo);
    void eliminarNodo(Nodo *nodo);
    void estimarEliminar(Nodo *arbol, int n);
};



#endif