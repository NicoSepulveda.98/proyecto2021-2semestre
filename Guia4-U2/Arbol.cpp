#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>
#include "Arbol.h"
using namespace std;

/* Constructor vacío */
Arbol::Arbol(){

}

/* Creación del nuevo Nodo */
Nodo *nodo(int i, Nodo *padre){
    Nodo *nodo = new Nodo();
    nodo->i = i;
    nodo->izq = NULL;
    nodo->der = NULL;
    nodo->padre = padre;
    return nodo;
}

/* Creación del grafo */
void Arbol::crearGrafo(Nodo *raiz){
    ofstream arch;
    /* Se abre el archivo datos.txt para luego escribir en el archivo
    cambiando el color a gusto personal comienza desde '{' hasta '}'  */
    arch.open("grafo.txt");
    arch << "digraph G {" << endl;
    /* Cambiar color */
    arch << "node [style=filled fillcolor=gray];" << endl;
    recorrerArbol(raiz, arch);
    /* Se termina de escribir dentro del archivo */
    arch << "}" << endl;
    arch.close();
    system("dot -Tpng -ografo.png grafo.txt &");
    system("eog grafo.png &");
}


/* Recorre el arbol el ofstream es el dato del archivo en cpp */
void Arbol::recorrerArbol(Nodo *nodo, ofstream &arch){
    string tmp = "\0";
    /* Enlazar los nodos */
    if (nodo != NULL){
        /* Tanto nodos que esten en la izquierda o derecha se debe
        escribir dentro del archivo */
        if (nodo->izq != NULL){
            arch << nodo->i << "->" << nodo->izq->i << ";" << endl;
            cout << "\n";
        }
        else {
            tmp = to_string(nodo->i) + "i";
            tmp = "\"" + tmp + "\"";
            arch << tmp << " [shape=point]" << endl;
            arch << nodo->i << "->" << tmp << ";" << endl;

        }
        tmp = nodo->i;
        if(nodo->der != NULL){
            arch << nodo->i << "->" << nodo->der->i << ";" << endl;
        }
        else {
            tmp = to_string(nodo->i) + "d";
            tmp = "\"" + tmp + "\"";
            arch << tmp << " [shape=point]" << endl;
            arch << nodo->i << "->" << tmp << ";" << endl;
        }
        /* Creación del grafo tanto por derecha e izquierda */
        recorrerArbol(nodo->izq, arch);
        recorrerArbol(nodo->der, arch);
    }
    return ;
}

/* Insertar Nodo */
void Arbol::insertarNodo(Nodo *&arbol, int n, Nodo *padre){
    /* Si esta vacío */
    if(arbol == NULL){
        /* Se llama a la función que crea el nodo */
        Nodo *nuevoNodo = nodo(n, padre);
        arbol = nuevoNodo;
    }
    /* Y si no lo esta obtenemos el valor de la raíz y
    calculamos si el elemento es menor o mayor para definir donde va
    o si es igual a la raíz no se incluye */
    else {
        int nRaiz = arbol->i;
        if (n < nRaiz){
            insertarNodo(arbol->izq, n, arbol);
        }
        else if (n > nRaiz){
            insertarNodo(arbol->der, n, arbol);
        }
        else {
            cout << "\nNúmero igual no permitido." << endl << endl;
        }
    }
}

/* Buscar un dato específico */
bool Arbol::busqueda(Nodo *arbol, int n){
    /* Si esta vacío */
    if (arbol == NULL){
        /* Retornamos a falso */
        return false;
    }
    else if (arbol->i == n){
        return true;
    }
    else if (n < arbol->i){
        return busqueda(arbol->izq, n);
    }
    else {
        return busqueda(arbol->der, n);
    }
}

/* Orden de preOrden */
void Arbol::preOrden(Nodo *arbol){
    /* Si esta vacío */
    if (arbol == NULL){
        /* Retorna a nada */
        return ;
    }
    else {
        cout << "[ " << arbol->i << " ]" << " ";
        preOrden(arbol->izq);
        preOrden(arbol->der);
    }
}

/* Orden inOrden */
void Arbol::inOrden(Nodo *arbol){
    /* Si esta vacío */
    if (arbol == NULL){
        /* Retorna a nada */
        return ;
    }
    else {
        inOrden(arbol->izq);
        cout << "[ " << arbol->i << " ]" << " ";
        inOrden(arbol->der);
    }
}

/* Orden posOrden */
void Arbol::posOrden(Nodo *arbol){
    /* Si esta vacío */
    if (arbol == NULL){
        /* Retorna a nada */
        return ;
    }
    else {
        posOrden(arbol->izq);
        posOrden(arbol->der);
        cout << "[ " << arbol->i << " ]" << " ";
    }
}

/* Destruir Nodo */
void Arbol::destruirNodo(Nodo *nodo){
    nodo->izq = NULL;
    nodo->der = NULL;
    nodo->padre = NULL;
    delete nodo;
}

/* Reemplazar Nodo para asignar nuevo hijo y nuevo padre */
void Arbol::reemplazarNodo(Nodo *arbol, Nodo *nuevoNodo){
    if (arbol->padre){
        /* Condicional de validación */
        if (arbol->padre->izq != NULL){
            if (arbol->i == arbol->padre->izq->i){
                arbol->padre->izq = nuevoNodo;
            }
        }
        /* Condicional de validación */
        if (arbol->padre->der != NULL){
            if (arbol->i == arbol->padre->der->i){
                arbol->padre->der = nuevoNodo;
            }
        }
    }
    if (nuevoNodo){
        nuevoNodo->padre = arbol->padre;
    }
    destruirNodo(arbol);
}

/* Determinar más a la izquierda de una posibilidad de tener hijos */
Nodo *minimo(Nodo *arbol){
    if (arbol == NULL){
        return NULL;
    }
    else if (arbol->izq){
        return minimo(arbol->izq);
    }
    else {
        /* Se retorna al mismo Nodo */
        return arbol;
    }
}

/* Eliminar nodo encontrado ya sea si tiene dos hijos
o si tiene uno izquierda o derecha */
void Arbol::eliminarNodo(Nodo *nodo){
    /* Si hay dos sub nodos izq y der */
    if (nodo->izq && nodo->der){
        Nodo *menor = minimo(nodo->der);
        nodo->i = menor->i;
        eliminarNodo(menor);
    }
    /* Si hay nodo izq */
    else if (nodo->izq){
        reemplazarNodo(nodo, nodo->izq);
        //destruirNodo(nodo);
    }
    /* Si hay nodo der */
    else if (nodo->der){
        reemplazarNodo(nodo, nodo->der);
        //destruirNodo(nodo);
    }
    /* No hay nodo o sub nodos */
    else {
        reemplazarNodo(nodo, NULL);
        //destruirNodo(nodo);
    }
}

/*  Eliminar por completo un Nodo */
void Arbol::estimarEliminar(Nodo *arbol, int n){
    /* Si esta vacío */
    if (arbol == NULL){
        /* Retornar a nada */
        return ;
    }
    else if (n < arbol->i){
        estimarEliminar(arbol->izq, n);
    }
    else if (n > arbol->i){
        estimarEliminar(arbol->der, n);
    }
    else {
        /* Y si se encuentra el elemento se elimina */
        eliminarNodo(arbol);
    }
}