#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>
#include "Arbol.h"
using namespace std;

void Menu(){
    Nodo *raiz = NULL;
    Arbol *g = new Arbol();
    int dato;
    int option;
    bool bandera = true;
    while (bandera == true){
        cout << endl;
        cout << "-----------------------MENU-----------------------" << endl;
        cout << "| [1] Insertar Nodo.                             | " << endl;
        cout << "| [2] Mostrar Arbol.                             |" << endl;
        cout << "| [3] Buscar Nodo.                               |" << endl;
        cout << "| [4] Ordenar en PreOrden, Inorden, Posorden.    |" << endl;
        cout << "| [5] Eliminar Nodo.                             |" << endl;
        cout << "| [6] Modificar Nodo.                            |" << endl;
        cout << "| [0] Salir.                                     |" << endl;
        cout << "--------------------------------------------------" << endl;
        cout << "\n¿Qué opción desea tomar?: ";
        cin >> option;
        if (option == 1){
            system("clear");
            cout << "Introduzca el número: ";
            cin >> dato;
            g->insertarNodo(raiz, dato, NULL);
            cout << endl << endl;
        }
        else if (option == 2){
            system("clear");
            cout << "Cargando la muestra del árbol..." << endl;
            g->crearGrafo(raiz);
        }
        else if (option == 3){
            system("clear");
            cout << "Introduzca el dato que desea buscar: ";
            cin >> dato;
            if (g->busqueda(raiz, dato) == true){
                cout << "Número: " << dato << " encontrado." << endl;
            }
            else {
                cout << "Número: " << dato <<" no encontrado." << endl;
            }
        }
        else if (option == 4){
            system("clear");
            cout << "Entrega de los tres orden." << endl << endl;
            cout << "PreOrden." << endl << endl;
            g->preOrden(raiz);
            cout << endl << endl << endl;
            cout << "InOrden." << endl << endl;
            g->inOrden(raiz);
            cout << endl << endl << endl;
            cout << "PosOrden." << endl << endl;
            g->posOrden(raiz);
            cout << endl << endl << endl;
        }
        else if (option == 5){
            system("clear");
            cout << "¿Qué dato desea eliminar?: ";
            cin >> dato;
            if (g->busqueda(raiz, dato)){
                g->estimarEliminar(raiz, dato);
            }
            else {
                cout << "Dato no encontrado." << endl;
            }
        }
        else if (option == 6){
            system("clear");
            cout << "¿Qué dato desea modificar?: ";
            cin >> dato;
            if(g->busqueda(raiz, dato)){
                g->estimarEliminar(raiz, dato);
                cout << "Ingrese dato nuevo: ";
                cin >> dato;
                if (g->busqueda(raiz, dato) == dato){
                    cout << "Número ya existente. Dato eliminado pero no modificado." << endl;
                }
                else{
                    g->insertarNodo(raiz, dato, NULL);
                }
            }
            else {
                cout << "\nNúmero no encontrado en el árbol. " << endl << endl;
            }
            //g->estimarEliminar(raiz, dato);
            //cout << "Ingrese dato nuevo: ";
            //cin >> dato;
            //g->insertarNodo(raiz, dato, NULL);
        }
        else if (option == 0){
            bandera = false;
        }
        else {
            cout << "\nNúmero incorrecto. Pruebe nuevamente." << endl << endl;
        }
    }
}

int main(){
    Menu();
    return 0;
}
