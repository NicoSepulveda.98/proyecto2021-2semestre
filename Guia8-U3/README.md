**Ejercicio Guia8-U3**

El siguiente programa consiste en un programa que va orientado a entregar números ordenados mediante métodos de ordenamiento, en este caso se mostraran los métodos de: Selección y Quicksort. Los números seran Random mediante un parámetro que se ingresará por terminal, los cuales se incorporarán y crearán una Array(lista).

**Especificaciones técnicas**

El tamaño de la matriz que se generará la entregará el usuario, es decir, al ejecutar el programa, por ejemplo por terminal, deberá ingresar un parámetro tipo entero mayor a 0 y menor a 1000000. El programa se ejecutará por Makefile, la ejecución es con el comando ./Programa (ingreso de parámetros). El primer parámetro debe ser número tipo entero que será el tamaño de la lista. Luego, en el segundo, debe ser tipo string en este caso se utilizará "s" o "S" para mostrar todos los valores ordenados y los milisegundos que tardó. Por otro lado si solo se quiere evidenciar el tiempo pero no los valores ordenados debe ingresar "n" o "N". 
Por ejemplo: **./Programa 10 S** o **./Programa 10 N**. 

**Historia:**

Fue escrito y desarrollado por Nicolás Sepúlveda Falcón, utilizando para ello el lenguaje de programación C++. Se trata de un lenguaje robusto fuertemente equipado y que sigue un paradigma de programación orientada a objetos.

**Para empezar:**

Es requisito tener instalado **C++**, de preferencia compilador **g++ 9.3.0**, previamente en su computadora, de lo contrario, el programa no podrá ser lanzado.
Recomendamos que el sistema operativo sobre el cual pretende lanzar el programa, corra sobre el kernel Linux (Debian, Ubuntu, Arch, entre otras.)

**Instalación:**

Para instalar y ejecutar el programa en su máquina, es necesario que siga las presentes indicaciones:
1- Clonar el repositorio "proyecto2021-2semestre" en el directorio de su preferencia, el enlace HTTPS para clonar el programa ingresando a la terminal es el siguiente:
```
git clone https://gitlab.com/NicoSepulveda.98/proyecto2021-2semestre.git
```
2- Luego, deberá entrar en la carpeta clonada que lleva por nombre "proyecto2021-2semestre" especificamente en "Guia8-U3" que contiene los ejercicios con sus respectivas clases en formato ".cpp" y ".h" y un Makefile.
3- Posteriormente, ya está usted preparado para ejecutar el programa. Puede ser lanzado desde una consola de su IDE de preferencia o desde una terminal en Linux

**Codificación:**

El programa soporta la codificación estándar UTF-8

**Construido con:**

Visual Studio Code, IDE utilizado por defecto para el desarrollo del proyecto.
Sitio web para descargar para Linux, Windows y Mac en https://code.visualstudio.com/download"

**Licencia:**

Este proyecto está sujeto bajo la Licencia GNU GPL v3.

**Autores y creadores del proyecto:**

> Nicolás Sepúlveda Falcón.

