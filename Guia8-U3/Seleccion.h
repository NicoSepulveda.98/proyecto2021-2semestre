#include <iostream>
using namespace std;
#include "Numero.h"
#ifndef SELECCION_H
#define SELECCION_H

class Seleccion {
    private:
    
    public:
    Seleccion();
    void OrdSeleccion(Numero *array, int n, int confirmacion);
    void imprimirNumeros(Numero *array, int n);
};

#endif