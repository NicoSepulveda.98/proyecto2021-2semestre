#include <iostream>
#include <time.h>
using namespace std;
#include "Numero.h"
#include "Quicksort.h"

Quicksort::Quicksort(){

}

/* Imprime los numeros de la array */
void Quicksort::imprimirNumeros(Numero *array, int n){
	for(int i = 0; i< n; i++){
		cout <<	"Array["<< i << "] = " << array[i].getValor() << "  ";
	}
	cout << endl;
}

/* Inicio Quicksort */ 
int Quicksort::divideQuicksort(Numero *array, int start, int end){
    // Variables necesarias e instanciar la clase Numero
    int left;
    int right;
    Numero pivot;
    Numero temp;

    pivot = array[start];
    left = start;
    right = end;

    while (left < right) {
        while (array[right].getValor() > pivot.getValor()) {
            right--;
        }

        while ((left < right) && (array[left].getValor() <= pivot.getValor())) {
            left++;
        }
        if (left < right) {
            temp = array[left];
            array[left] = array[right];
            array[right] = temp;
        }
    }
    temp = array[right];
    array[right] = array[start];
    array[start] = temp;
    return right;
}

void Quicksort::quicksort(Numero *array, int start, int end){
    int pivot;

    if (start < end) {
        pivot = divideQuicksort(array, start, end);
        
        quicksort(array, start, pivot - 1);

        quicksort(array, pivot + 1, end);
    }
}