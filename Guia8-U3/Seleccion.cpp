#include <iostream>
#include <time.h>
#include "Seleccion.h"
#include "Numero.h"
using namespace std;

Seleccion::Seleccion(){
}

// Imprime los numeros de la array
void Seleccion::imprimirNumeros(Numero *array, int n){
	for(int i = 0; i< n; i++){
		cout <<	"Array["<< i << "] = " << array[i].getValor() << "  ";
	}
	cout << endl;
}

void Seleccion::OrdSeleccion(Numero *array, int n, int confirmacion){
	double tiempo = 0;
	clock_t comienzo;
	comienzo = clock();
	Numero menor;
	int aux;
	for (int i = 0; i < n - 1; i++){
		menor = array[i];
		aux = i;
		for (int j = i + 1; j < n; j ++){
			if (array[j].getValor() < menor.getValor()){
				menor = array[j];
				aux = j;
			}
		}
		array[aux] = array[i];
		array[i] = menor;
		
	}
	// Muestra el tiempo de ejecución si es el parámetro es n o N 
	if(confirmacion == 0){ // Confirmación se vuelve 0 con parámetri n o N
	tiempo = (clock()-comienzo)/(double)CLOCKS_PER_SEC * 1000;
	cout << "[Time-Selección]:    " << tiempo << " milisegundos" << endl;
	}
	// Si no es así, muestra el tiempo y valores
	else {
	tiempo = (clock()-comienzo)/(double)CLOCKS_PER_SEC * 1000;
	cout << "[Time-Selección]:    " << tiempo << " milisegundos" << endl;
	cout << "[Selección]:         ";
	imprimirNumeros(array, n);
	}
}