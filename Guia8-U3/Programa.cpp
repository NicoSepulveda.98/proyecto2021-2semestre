#include "Numero.h"
#include "Quicksort.h"
#include "Seleccion.h"
#include <iostream>
#include <time.h>
using namespace std;

/* Imprime los numeros de la array */
void imprimirNumeros(Numero *array, int n){
	for(int i = 0; i< n; i++){
		cout <<	"Array["<< i << "] = " << array[i].getValor() << "  ";
	}
	cout << endl;
}

/* Armar registro de números */
void registroNumero(Numero *array, int n, int confirmacion){
	// Rellenar con números aleatorios de la array
	for(int i = 0; i < n ; i ++){
			array[i].setValor(1 + rand() % 10);
	}
	// Imprimir array
	cout << "[Números]:           ";
	imprimirNumeros(array, n);
	cout << endl << endl;

}

/* Entregar números a cada array */
void copiarArreglo(Numero *array, int n, int confirmacion){
	Seleccion selection;
	Quicksort quicks;	
	// Crear arreglos no dinámicos para cada orden
	Numero *array1 = new Numero[n];
	Numero *array2 = new Numero[n];
	// Asignar mismos números para ordenarlos
	array1 = array;
	array2 = array;

	// Ordenamientos con sus respectivos números (iguales)
    selection.OrdSeleccion(array1, n, confirmacion);
    cout << endl;
	
	// Impresiones Quicksort
	double tiempo = 0;
	clock_t comienzo;
	comienzo = clock();
	quicks.quicksort(array2, 0, n - 1);

	if(confirmacion == 0){
		tiempo = (clock() - comienzo) / (double)CLOCKS_PER_SEC * 1000;
		cout << "[Time-Quicksort]:    " << tiempo << " milisegundos" << endl;
	}
	else {
		tiempo = (clock() - comienzo) / (double)CLOCKS_PER_SEC * 1000;
		cout << "[Time-Quicksort]:    " << tiempo << " milisegundos" << endl;
		cout << "[Quicksort]:         ";
		imprimirNumeros(array, n);
	}
    cout << endl;
}

/* Llama a los métodos de ordenamientos */
void control(Numero *array, int n, int confirmacion){
	cout << "[Métodos de Ordenamientos]  " << endl;
	cout << endl;
	// Rellena el arreglo aleatoriamente
	registroNumero(array, n, confirmacion);
	copiarArreglo(array, n, confirmacion);
}

/* Main */
int main(int argc, char *argv[]){
	// Verifica la entrada de parámetros, si no hay se finaliza
	if(argv[0] == NULL || argv[1] == NULL || argv[2] == 0){
		cout<< "[No Ingresó Parámetros]" << endl;
		return 0;
	}
	try{
		char n = stoi(argv[1]);
	}
	catch (const std::invalid_argument& e){
		cout << "[El primer parámetro debe ser un número]" << endl;
		exit(1);
	}
	// Fin validación de erróres

	// Crear números random y se entrega el tamaño del arreglo
	srand(time(0));
	int n = stoi(argv[1]);
	string ver = argv[2];
	int confirmacion;
	Numero *array = new Numero[n];

	// Se válida el tamaño del arreglo
	if(n > 1000000 or n <= 0){
		cout<< "Parámetro Numérico no válido." << endl;
		return 0;
	}
	if (ver == "s" or ver == "S"){
		confirmacion = 1; 
		control(array, n, confirmacion);
	}
	else if (ver == "n" or ver == "N"){
		confirmacion = 0;
		control(array, n, confirmacion);
	}
	else {
		cout << "[No ingresó el parámetro VER]" << endl;
		return 0;
	}

	return 0;
}
