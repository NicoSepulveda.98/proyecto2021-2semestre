#include <iostream>
using namespace std;
#include "Numero.h"
#ifndef QUICKSORT_H
#define QUICKSORT_H

class Quicksort {
    private:
    
    public:
    /* Constructor */
    Quicksort();
    void imprimirNumeros(Numero *array, int n);
    int divideQuicksort(Numero *array, int start, int end);
    void quicksort(Numero *array, int start, int end);
};
#endif