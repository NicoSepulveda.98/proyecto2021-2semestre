#include <iostream>
using namespace std;

#ifndef NUMERO_H
#define NUMERO_H

class Numero {
    private:
    // Atributos privados 
    int valor = 0; // Instanciar una variable tipo entero igual a 0

    public:
    // Constructor
    Numero();
    // get y setter (valores)
    int getValor();
    void setValor(int valor);

};

#endif