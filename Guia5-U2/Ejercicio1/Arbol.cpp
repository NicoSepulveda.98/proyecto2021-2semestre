#include <iostream>
#include <unistd.h>
#include <fstream>
using namespace std;
#include "Arbol.h"

/* Constructor vacío */
Arbol::Arbol(){
    
}

/* Crear Nodo */
Nodo *Arbol::crearNodo(string n, Nodo *padre){
    Nodo *aux;
    aux = new Nodo();
    aux->dato = n;
    aux->izq = NULL;
    aux->der = NULL;
    aux->padre = padre;

    return aux;
}

/* Creación del grafo */
void Arbol::crearGrafo(Nodo *raiz){
    ofstream arch;
    /* Se abre el archivo datos.txt para luego escribir en el archivo
    cambiando el color a gusto personal comienza desde '{' hasta '}'  */
    arch.open("grafo.txt");
    arch << "digraph G {" << endl;
    /* Cambiar color */
    arch << "node [style=filled fillcolor=gray];" << endl;
    recorrerArbol(raiz, arch);
    /* Se termina de escribir dentro del archivo */
    arch << "}" << endl;
    arch.close();
    system("dot -Tpng -ografo.png grafo.txt &");
    system("eog grafo.png &");
}


/* Recorre el arbol el ofstream es el dato del archivo en cpp */
void Arbol::recorrerArbol(Nodo *nodo, ofstream &arch){
    /* Enlazar los nodos */
    if (nodo != NULL){
        /* Tanto nodos que esten en la izquierda o derecha se debe
        escribir dentro del archivo */
        arch << "\n" << '"' << nodo->dato << '"' << "->" << '"' << nodo->dato << '"' << "[label=" << nodo->infoEquilibrado << "]" << ";" << endl;
        if (nodo->izq != NULL){
            arch << "\n" << '"' << nodo->dato << '"' << "->" << '"' << nodo->izq->dato << '"' << ";" << endl;
        }
        else {
            arch << "\n" << '"' << nodo->dato << "i" << '"' << " [shape=point];" << endl;
            arch << "\n" << '"' << nodo->dato << '"' << "->" << '"' << nodo->dato << "i" << '"' << endl;

        }
        if(nodo->der != NULL){
            arch << "\n" << '"' << nodo->dato << '"' << "->" << '"' << nodo->der->dato << '"' << ";" << endl;
        }
        else {
            arch << "\n" << '"' << nodo->dato << "d" << '"' << " [shape=point];" << endl;
            arch << "\n" << '"' << nodo->dato << '"' << "->" << '"' << nodo->dato << "d" << '"' << endl;
        }
        /* Creación del grafo tanto por derecha e izquierda */
        recorrerArbol(nodo->izq, arch);
        recorrerArbol(nodo->der, arch);
    }
    return ;
}

/* Rotación de las ramas derecha */
void Arbol::rotacionDD(Nodo *&raiz, bool booleano, Nodo *nodo){
    raiz->der = nodo->izq;
    nodo->izq = raiz;

    /* Actualizar */
    if (nodo->infoEquilibrado == 0){
        raiz->infoEquilibrado = 1;
        nodo->infoEquilibrado = -1;
        booleano = false;
    }
    else if (nodo->infoEquilibrado == 1){
        raiz->infoEquilibrado = 0;
        nodo->infoEquilibrado = 0;
    }
    /* Asignar un nuevo valor a la raíz */
    raiz = nodo;
}

/* Rotación a la derecha y luego izquierda */
void Arbol::rotacionDI(Nodo *&raiz, Nodo *nodo, Nodo *nodo1){
    nodo1 = nodo->izq;
    raiz->der = nodo1->izq;
    nodo1->izq = raiz;
    nodo->izq = nodo1->der;
    nodo1->der = nodo;

    if (nodo1->infoEquilibrado == 1){
        raiz->infoEquilibrado = -1;
    }
    else {
        raiz->infoEquilibrado = 0;
    }
    if (nodo1->infoEquilibrado == -1){
        nodo->infoEquilibrado = 1;
    }
    else {
        nodo->infoEquilibrado = 0;
    }
    raiz = nodo1;
    nodo1->infoEquilibrado = 0;
}

/* Rotación a la izquierda */
void Arbol::rotacionII(Nodo *&raiz, bool booleano, Nodo *nodo){
    raiz->izq = nodo->der;
    nodo->der = raiz;

    if (nodo->infoEquilibrado == 0){
        raiz->infoEquilibrado = -1;
        nodo->infoEquilibrado = 1;
        booleano = false;
    }
    else if (nodo->infoEquilibrado == -1){
        raiz->infoEquilibrado = 0;
        nodo->infoEquilibrado = 0;
    }
    /* Se asigna un nuevo valor para la raiz */
    raiz = nodo;
}

/* Rotación izquierda y derecha */
void Arbol::rotacionID(Nodo *&raiz, Nodo *nodo, Nodo *nodo1){
    nodo1 = nodo->der;
    raiz->izq = nodo1->der;
    nodo1->der = raiz;
    nodo->der = nodo1->izq;
    nodo1->izq = nodo;

    if (nodo1->infoEquilibrado == -1){
        raiz->infoEquilibrado = 1;
    }
    else {
        raiz->infoEquilibrado = 0;
    }
    if (nodo1->infoEquilibrado == 1){
        nodo->infoEquilibrado = -1;
    }
    else {
        nodo->infoEquilibrado = 0;
    }

    raiz = nodo1;
    nodo1->infoEquilibrado = 0;
}

void Arbol::reestructuracionIzq(bool booleano, Nodo *&raiz){
    Nodo *nodo;
    Nodo *nodo1;

    if (booleano == true){
        /* Verificar el valor equilibrio */
        if (raiz->infoEquilibrado == -1){
            raiz->infoEquilibrado = 0;
        }
        else if (raiz->infoEquilibrado == 0){
            raiz->infoEquilibrado = 1;
            booleano = false;
        }
        else if (raiz->infoEquilibrado == 1){
            nodo = raiz->der;
            /* Identificar rotación */
            if (nodo->infoEquilibrado >= 0){
                rotacionDD(raiz, booleano, nodo);
            }
            else {
                rotacionDI(raiz, nodo, nodo1);
            }
        }
    }
}

void Arbol::reestructuracionDer(bool booleano, Nodo *&raiz){
    Nodo *nodo;
    Nodo *nodo1;
    if (booleano == true){
        /* Verificar el valor de equilibrio */
        if(raiz->infoEquilibrado == 1){
            raiz->infoEquilibrado = 0;
        }
        else if (raiz->infoEquilibrado == 0){
            raiz->infoEquilibrado = -1;
            booleano = false;
        }
        else if (raiz->infoEquilibrado == -1){
            nodo = raiz->izq;
            /* Se identifica rotación */
            if (nodo->infoEquilibrado <= 0){
                rotacionII(raiz, booleano, nodo);
            }
            else {
                rotacionID(raiz, nodo, nodo1);
            }
        }
    }
}

void Arbol::insertar(Nodo *&raiz, string n, bool &booleano, Nodo *padre){
    string vRaiz;
    
    if (raiz == NULL){
        Nodo *aux;
        aux = crearNodo(n, padre);
        raiz = aux;
        insertar(raiz, n, booleano, padre);
    }
    else{
        vRaiz = raiz->dato;
        /* Insertar nodo en el subárbol izquierdo */
        if(n < vRaiz){
            /* Agregación de nuevos nodos */
            insertar(raiz->izq, n, booleano, raiz);
            reestructuracionDer(booleano, raiz);
        }
        /* Insertar nodo en el subárbol derecho */
        else if (n > vRaiz){
            /* Agregación de nuevos nodos */
            insertar(raiz->der, n, booleano, raiz);
            reestructuracionIzq(booleano, raiz);
        }
        else {
            cout << "\nEl dato: " << n << " , se insertó en el árbol" << endl;
        }
    }
    booleano = true;
}

void Arbol::eliminar(Nodo *&raiz, bool &booleano, string n){
    Nodo *otro;
    Nodo *aux1;
    Nodo *aux2;
    bool bandera;

    if (raiz != NULL){
        /* Se identifica el subárbol */
        if (n < raiz->dato){
            /* Se elimina */
            eliminar(raiz->izq, booleano, n);
            /* Se reestructura */
            reestructuracionIzq(booleano, raiz);
        }
        else if (n > raiz->dato){
            eliminar(raiz->der, booleano, n);
            reestructuracionDer(booleano, raiz);
        }
        else {
            otro = raiz;
            /* Si el subárbol derecho esta vacío */
            if (otro->der == NULL){
                raiz = otro->izq;
            }
            else {
                /* Si el subárbol izquierdo vacío */
                if (otro->izq == NULL){
                    raiz = otro->der;
                }
                /* Elementos con dos hijos */
                else {
                    aux1 = raiz->izq;
                    bandera = false;
                    
                    /* Se sustituye por el nodo más a la derecha */
                    while (aux1->der != NULL){
                        aux2 = aux1;
                        aux1 = aux1->der;
                        bandera = true;
                    }
                    /* Se cambia el numero por raíz */
                    raiz->dato = aux1->dato;
                    otro = aux1;

                    if (bandera == true){
                        aux2->der = aux1->izq;
                    }
                    else {
                        raiz->izq = aux1->izq;
                        /* Limpiar memoria */
                        free(otro);
                    }
                }
            }
        }
    }
    else {
        cout << "\nNo se encuentra en el árbol." << endl;
    }
}

/* Buscar un dato específico */
bool Arbol::busqueda(Nodo *arbol, string n){
    /* Si esta vacío */
    if (arbol == NULL){
        /* Retornamos a falso */
        return false;
    }
    else if (arbol->dato == n){
        return true;
    }
    else if (n < arbol->dato){
        return busqueda(arbol->izq, n);
    }
    else {
        return busqueda(arbol->der, n);
    }
}
