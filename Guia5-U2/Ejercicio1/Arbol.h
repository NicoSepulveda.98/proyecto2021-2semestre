#include <iostream>
#include <unistd.h>
using namespace std;
#ifndef ARBOL_H
#define ARBOL_H

typedef struct _Nodo{
    string dato;
    int infoEquilibrado;
    struct _Nodo *izq;
    struct _Nodo *der;
    struct _Nodo *padre;
} Nodo;

class Arbol{
    private:

    public:
    /* Constructor */
    Arbol();
    void crearGrafo(Nodo *raiz);
    void recorrerArbol(Nodo *nodo, ofstream &arch);
    Nodo *crearNodo(string n, Nodo *padre);
    void insertar(Nodo *&raiz, string n, bool &booleano, Nodo *padre);
    void eliminar(Nodo *&raiz, bool &booleano, string n);
    /* Rotación reestructuración izquierda */ 
    void rotacionDD(Nodo *&raiz, bool booleano, Nodo *nodo);
    void rotacionDI(Nodo *&raiz, Nodo *nodo, Nodo *nodo1);
    void reestructuracionIzq(bool booleano, Nodo *&raiz);
    /* Rotación reestructuración derecha */
    void rotacionII(Nodo *&raiz, bool booleano, Nodo *nodo);
    void rotacionID(Nodo *&raiz, Nodo *nodo, Nodo *nodo1);
    void reestructuracionDer(bool booleano, Nodo *&raiz);
    bool busqueda(Nodo *arbol, string n);

};

#endif