#include <iostream>
#include <stdio.h>
#include <string.h>
#include <fstream>
#include "Arbol.h"
using namespace std;

void menu(){
    /* Se crea objeto de clase */
    Arbol *arbol = new Arbol();
    Nodo *raiz = NULL;
    int option;
    string dato;
    bool booleano = false;

    while (option != 5){
        cout << endl;
        cout << "---------- MENU ----------" << endl;
        cout << "|                        |" << endl;
        cout << "| [1] Ingresar ID.       |" << endl;
        cout << "| [2] Eliminar ID.       |" << endl;
        cout << "| [3] Modificar ID.      |" << endl;
        cout << "| [4] Mostrar árbol.     |" << endl;
        cout << "| [5] Salir              |" << endl;
        cout << "|                        |" << endl;
        cout << "--------------------------" << endl;
        cout << "\n  ¿Qúe opción desea tomar?: ";
        cin >> option;

        if (option == 1){
            system("clear");
            cout << "Ingrese el dato: ";
            cin >> dato;
            if(arbol->busqueda(raiz, dato)){
                cout << "\nYa existe el dato en el árbol" << endl;
            }
            else {
                arbol->insertar(raiz, dato, booleano, NULL);
            }
        }
        else if (option == 2){
            system("clear");
            cout << "Digite el dato que eliminará: ";
            cin >> dato;
            if (arbol->busqueda(raiz, dato)){
                arbol->eliminar(raiz, booleano, dato);
                cout << "\nDato eliminado." << endl;
            }
            else {
                cout << "\nDato no encontrado." << endl;
            }
        }
        else if (option == 3){
            system("clear");
            cout << "¿Qué dato desea modificar?: ";
            cin >> dato;
            if (arbol->busqueda(raiz, dato)){
                arbol->eliminar(raiz, booleano, dato);
                cout << "\nNuevo dato: ";
                cin >> dato;
                if (arbol->busqueda(raiz, dato)){
                    cout << "\nDato ya existente. El dato a reemplazar fue eliminado pero no modificado." << endl;
                }
                else {
                    arbol->insertar(raiz, dato, booleano, NULL);
            }
            }
            else {
                cout << "\nNo existe el dato en el árbol." << endl;
            }
        }
        else if (option == 4){
            arbol->crearGrafo(raiz);
        }
        else if (option == 5){
            cout << "\nPROGRAMA FINALIZADO." << endl << endl;
        }
        /*
        else if (option == 6){
            archivo.open("pdb.txt");
            if (archivo.fail()) {
                cout << "Error al abrir el archivo." << endl;
                exit(1);
            }
            while (!archivo.eof()) {
                getline(archivo, codigo_pdb);
                arbol.insertar(raiz, dato, booleano, NULL);
            }
            arbol.crearGrafo(raiz);
        }
        */
        else {
            cout << "\nOpción incorrecta. Digite otra opción." << endl << endl;
        }
    }
}

int main(void){
    menu();
    return 0;
}